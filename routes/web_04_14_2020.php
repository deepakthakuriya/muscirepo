<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    return view('auth.login');
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('migrate');
    $exitCode = Artisan::call('optimize');
    return 'DONE'; //Return anything
});

Auth::routes();



Route::group(['middleware' => ['auth'], 'namespace' => 'Backend'], function() {

    //['middleware' => ['permission:publish articles|edit articles']], function
//->middleware(['permission:User View|User Edit']);

	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');


    //User Master
    Route::group(['middleware' => ['permission:User Master']], function() {
        Route::get('/users', 'UserController@index')->name('user-list')->middleware(['permission:User List']);
        Route::get('/users/create', 'UserController@create')->name('user-create')->middleware(['permission:User Create']);
        Route::post('/users/store', 'UserController@store')->name('user-save')->middleware(['permission:User Create']);
        Route::get('/users/edit/{id}', 'UserController@edit')->name('user-edit')->middleware(['permission:User Edit']);
        Route::post('/users/update/{id}', 'UserController@update')->name('user-update')->middleware(['permission:User Edit']);
        Route::get('/ajax/users/view/{id}', 'UserController@show')->name('user-view')->middleware(['permission:User View']);
    });

	//Business Master
    Route::group(['middleware' => ['permission:Business Master']], function() {
        Route::get('/business/list', 'BusinessController@index')->name('business-list')->middleware(['permission:Business List']);
        Route::get('/business/create', 'BusinessController@create')->name('business-create')->middleware(['permission:Business Create']);
        Route::Post('/business/store', 'BusinessController@store')->name('business-store')->middleware(['permission:Business Create']);
        Route::get('/business/edit/{id}', 'BusinessController@edit')->name('business-edit')->middleware(['permission:Business Edit']);
        Route::Post('/business/update/{id}', 'BusinessController@update')->name('business-update')->middleware(['permission:Business Edit']);
       });

	 //Store Master
    Route::group(['middleware' => ['permission:Store Master']], function() {
        Route::get('/store/list', 'StoreController@index')->name('store-list')->middleware(['permission:Store List']);
        Route::get('/store/create', 'StoreController@create')->name('store-create')->middleware(['permission:Store Create']);
        Route::Post('/store/store', 'StoreController@store')->name('store-store')->middleware(['permission:Store Create']);
        Route::get('/store/edit/{id}', 'StoreController@edit')->name('store-edit')->middleware(['permission:Store Edit']);
        Route::Post('/store/update/{id}', 'StoreController@update')->name('store-update')->middleware(['permission:Store Edit']);
       });

    //Offer Master
       Route::group(['middleware' => ['permission:Offer Master']], function() {
       Route::get('/offer/list', 'OfferController@index')->name('offer-list')->middleware(['permission:Offer List']);
       Route::get('/offer/create', 'OfferController@create')->name('offer-create')->middleware(['permission:Offer Create']);
       Route::Post('/offer/store', 'OfferController@store')->name('offer-store')->middleware(['permission:Offer Create']);
       Route::get('/offer/edit/{id}', 'OfferController@create')->name('offer-edit')->middleware(['permission:Offer Edit']);
       // Route::Post('/offer/update/{id}', 'OfferController@update')->name('offer-update')->middleware(['permission:Offer Edit']);
       Route::get('/offer/offer-validate/', 'OfferController@offerValidate')->name('offer-validate')->middleware(['permission:Offer Validate']);
       Route::Post('/offer/coupousers', 'OfferController@CouponUserSave')->name('coupon-users-save')->middleware(['permission:Offer Create']);
       });

       	 //Coupon Users
     Route::group(['middleware' => ['permission:Coupon Users']], function() {
        Route::get('/coupon-users/list', 'CouponUsersController@index')->name('coupon-users-list')->middleware(['permission:Coupon Users List']);
        Route::get('/coupon-users/validate', 'CouponUsersController@offerValidate')->name('coupon-users-validate')->middleware(['permission:Validate Coupon']);
       });



      Route::get('/get-stores', 'StoreController@getStores');
      Route::get('/get-stores-length', 'StoreController@getStoresLength');

      Route::get('/get-coupon-check', 'OfferController@getCouponCheck');

      Route::get('/get-stores-business', 'OfferController@getStoresBusiness');
      Route::get('/get-stores', 'OfferController@getAllStoresInOffer');

      Route::get('/setting', 'SettingController@index')->name('setting');
      Route::post('/setting/password/update', 'SettingController@updatePassword')->name('password-update');
      Route::post('/logout', 'UserController@logout')->name('logout');

    Route::get('/roles-list', 'RolePermissionController@roles')->name('roles-list');
    Route::get('/roles/create', 'RolePermissionController@create')->name('roles-create');
    Route::post('/roles/store', 'RolePermissionController@store')->name('roles-store');
    Route::get('/roles/edit/{id}', 'RolePermissionController@edit')->name('roles-edit');
    Route::post('/roles/update/{id}', 'RolePermissionController@update')->name('roles-update');
    Route::get('/ajax/roles/view/{id}', 'RolePermissionController@show')->name('roles-view');


});
