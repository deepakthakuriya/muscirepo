<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

Route::group(['namespace' => 'Api\Mobile\Auth'], function () {

    # Manually Registration
    Route::post('register', 'OnBoardingController@register');
    # Registration By Phone
    Route::post('register-phone', 'OnBoardingController@signupPhone');
    Route::post('verify-otp', 'OnBoardingController@verifyOTP');
    Route::post('resend-otp', 'OnBoardingController@resendOTP');
    # Login By E-mail
    Route::post('login', 'OnBoardingController@login');
    # Login with Social Login
    Route::post('social-login', 'OnBoardingController@socialLogin');

});



Route::group(['namespace' => 'Api\Mobile\Auth'], function () {

    Route::post('category', 'CommonController@category');
    Route::post('genres', 'CommonController@genres');
    Route::post('languages', 'CommonController@languages');
    Route::post('pitch', 'CommonController@pitch');
    Route::post('themes', 'CommonController@themes');
    Route::post('moods', 'CommonController@moods');    
    Route::post('vocal-types', 'CommonController@vocalTypes');
    Route::post('tempos', 'CommonController@tempos');
    Route::post('instruments', 'CommonController@instruments');
    Route::post('bpms', 'CommonController@bpms');

});

//auth:api
Route::group(['prefix'=>'auth','middleware' => 'auth:api','namespace' => 'Api\Mobile\Auth'], function() {

    Route::post('choose-generes', 'MusicController@choosegeneres');

    # Profile Apis
    Route::post('/get-profile', 'MusicController@getProfile');
    Route::put('/update-profile', 'MusicController@updateProfile');
    Route::post('/upload-image',  'MusicController@uploadImage');

    # Like Music Apis
    Route::post('/likes-music',  'MusicController@likesMusic');
    Route::post('/liked-music',  'MusicController@getLikedMusic');
    
    # Uploaded Music
    Route::put('/liked-music',  'MusicController@getLikedMusic');

    # Follow User
    Route::post('/follow-user',  'MusicController@followUser');
    Route::post('/following-list',  'MusicController@followingLists');
    Route::post('/follower-list',  'MusicController@followerLists');
    
    # Notification
    Route::get('/get-notification',  'MusicController@getNotification');
    Route::put('/update-notification',  'MusicController@updateNotification');

    # Searching By Language ID, PitchID, TempId eg. [1,2]
    Route::post('/search-music',  'MusicController@searchMusic');
    
    # Send Offers
    Route::post('/send-offer',  'MusicController@sendOffer');

    # Trending
    Route::post('/get-trending',  'MusicController@getTrending');
    Route::post('/list-trending',  'MusicController@listTrending');
    
    # Radio
    Route::post('/get-radio',  'MusicController@getRadio');

    # Radio
    Route::post('/get-subscription',  'MusicController@getSubscriptions');


});
