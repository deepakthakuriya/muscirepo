<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 // get the authenticated User
Route::get('/test', function () {
    return view('auth.login');
});

Route::get('/forgot-password', function () {
    return view('auth.forgot-password');
})->name('forgot-password');

Route::post('/send-email', 'Backend\SettingController@ForgotPassword')->name('send-email');


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('migrate');
    $exitCode = Artisan::call('optimize');
    return 'DONE'; //Return anything
});

Auth::routes();


	Route::get('/cron', 'Backend\LeadsController@CronJob')->name('cron.job');
	
Route::group(['middleware' => ['auth'], 'namespace' => 'Backend'], function() {


	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');


    //User Master
    Route::group(['middleware' => ['permission:User Master']], function() {
        Route::get('/users', 'UserController@index')->name('user-list')->middleware(['permission:User List']);
        Route::get('/users/create', 'UserController@create')->name('user-create')->middleware(['permission:User Create']);
        Route::post('/users/store', 'UserController@store')->name('user-save')->middleware(['permission:User Create']);
        Route::get('/users/edit/{id}', 'UserController@edit')->name('user-edit')->middleware(['permission:User Edit']);
        Route::get('/users/view/{id}', 'UserController@show')->name('user-views')->middleware(['permission:User Edit']);
        Route::post('/users/update/{id}', 'UserController@update')->name('user-update')->middleware(['permission:User Edit']);
        Route::get('/ajax/users/view/{id}', 'UserController@show')->name('user-view')->middleware(['permission:User View']);
        
        Route::get('/cities-list', 'UserController@CityLength')->name('cities-list');
    });

    //Category Master
    Route::group(['middleware' => ['permission:Category Master']], function() {
        Route::get('/admin/category', 'CategoryController@index')->name('category-list')->middleware(['permission:Category List']);
        Route::get('/admin/category/create', 'CategoryController@create')->name('category-create')->middleware(['permission:Category Create']);
        Route::post('/admin/category/store', 'CategoryController@store')->name('category-save')->middleware(['permission:Category Create']);
        Route::get('/admin/category/edit/{id}', 'CategoryController@edit')->name('category-edit')->middleware(['permission:Category Edit']);
        Route::post('/admin/category/update/{id}', 'CategoryController@update')->name('category-update')->middleware(['permission:Category Edit']);
        Route::get('/admin/ajax/category/view/{id}', 'CategoryController@show')->name('category-view')->middleware(['permission:Category View']);
    });

   //Leads Master
    Route::group(['middleware' => ['permission:Leads Master']], function() {
        Route::get('/admin/leads', 'LeadsController@index')->name('leads-list')->middleware(['permission:Leads List']);

        Route::get('/admin/open-leads', 'LeadsController@openLeads')->name('open-list')->middleware(['permission:Leads List']);
        Route::get('/admin/closed-leads', 'LeadsController@closedLeads')->name('closed-list')->middleware(['permission:Leads List']);
        Route::get('/admin/re-scheduled-leads', 'LeadsController@rescheduledLeads')->name('re-scheduled-list')->middleware(['permission:Leads List']);
        Route::get('/admin/cancelled-leads', 'LeadsController@cancelledLeads')->name('cancelled-list')->middleware(['permission:Leads List']);
         Route::get('/admin/first-Contact-list', 'LeadsController@firstContactlist')->name('first-Contact-list')->middleware(['permission:Leads List']);
        


        Route::get('/admin/leads/create', 'LeadsController@create')->name('leads-create')->middleware(['permission:Leads Create']);
        Route::post('/admin/leads/store', 'LeadsController@store')->name('leads-save')->middleware(['permission:Leads Create']);
        Route::get('/admin/leads/edit/{id}', 'LeadsController@edit')->name('leads-edit')->middleware(['permission:Leads Edit']);
        Route::post('/admin/leads/update/{id}', 'LeadsController@update')->name('leads-update')->middleware(['permission:Leads Edit']);
        Route::get('/admin/ajax/leads/view/{id}', 'LeadsController@show')->name('leads-view')->middleware(['permission:Leads View']);
        Route::get('/admin/leads/details/{id}', 'LeadsController@LeadsDetails')->name('lead-details')->middleware(['permission:Leads View']);
        Route::post('/admin/leads/details/update/{id}', 'LeadsController@LeadsDetailsUpdate')->name('lead-details-update')->middleware(['permission:Leads View']);
        //Zonal Leads
        Route::get('/admin/zonal', 'LeadsController@ZonalLeadsList')->name('zone-lead')->middleware(['permission:Leads View']);
        
        Route::post('/admin/leads/export-data', 'LeadsController@ExportData')->name('export-data')->middleware(['permission:Leads View']);
        Route::post('/admin/leads/status/update/{id}', 'LeadsController@Statusupdate')->name('status.update')->middleware(['permission:Leads Edit']);
        Route::get('/admin/leads/substatus', 'LeadsController@getsubstatus')->name('get.substatus')->middleware(['permission:Leads View']);
    });

    //Escalation Master
    Route::group(['middleware' => ['permission:Escalation Master']], function() {
        Route::get('/admin/escalation', 'EscalationController@index')->name('escalation-list')->middleware(['permission:Escalation List']);
        Route::get('/admin/escalation/create', 'EscalationController@create')->name('escalation-create')->middleware(['permission:Escalation Create']);
        Route::post('/admin/escalation/store', 'EscalationController@store')->name('escalation-save')->middleware(['permission:Escalation Create']);
        Route::get('/admin/escalation/edit/{id}', 'EscalationController@edit')->name('escalation-edit')->middleware(['permission:Escalation Edit']);
        Route::post('/admin/escalation/update/{id}', 'EscalationController@update')->name('escalation-update')->middleware(['permission:Escalation Edit']);
    });
    
    

      
      Route::get('/setting', 'SettingController@index')->name('setting');
      Route::post('/setting/password/update', 'SettingController@updatePassword')->name('password-update');
      Route::post('/logout', 'UserController@logout')->name('logout');

  
    
    Route::get('/roles-list', 'RolePermissionController@roles')->name('roles-list')->middleware(['permission:Roles Create']);
    Route::get('/roles/create', 'RolePermissionController@create')->name('roles-create')->middleware(['permission:Roles List']);
    Route::post('/roles/store', 'RolePermissionController@store')->name('roles-store')->middleware(['permission:Roles Create']);
    Route::get('/roles/edit/{id}', 'RolePermissionController@edit')->name('roles-edit')->middleware(['permission:Roles Create']);
    Route::post('/roles/update/{id}', 'RolePermissionController@update')->name('roles-update')->middleware(['permission:Roles List']);
    Route::get('/ajax/roles/view/{id}', 'RolePermissionController@show')->name('roles-view')->middleware(['permission:Roles View']);
    
   

});


