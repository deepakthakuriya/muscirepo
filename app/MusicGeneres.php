<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicGeneres extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'musicgenres';

     protected $fillable = ['id','musicId','genreId'];


}
