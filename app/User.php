<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    public $timestamps = false;

    protected $fillable = ['name', 'email', 'password','role_id','createdDate','updatedDate'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function user_detail()
    {
       return $this->hasOne('App\User', 'id', 'user_id');
    }

	public function created_detail()
    {
       return $this->hasOne('App\User', 'id', 'created_by');
    }

	public function rolesss()
    {
       return $this->hasOne('App\Role', 'id', 'role_id');
    }


    public function user_business_detail()
    {
       return $this->hasOne('App\User', 'business_id');
    }

    

    public static function userDetails($user_id)
    {
        $query = User::where('id', '=', $user_id);
        $query = $query->where('active', '!=', 'DELETED');
        //$query = User::getUserColumns($query);
        $query = $query->first();

        return $query;
    }

    public static function addNotification($userID)
    {
        $notification = new NotificationSettings();
        $notification->userId = $userID;
        $notification->push = 'Y';
        $notification->autoPlay = 'Y';
        $notification->likesOnTune = 'Y';
        $notification->newFollowers = 'Y';
        $notification->newFeatures = 'Y';
        $notification->suggestedContent = 'Y';
        $notification->newPromotions = 'Y';
        $notification->save();

        return $notification;
    }

    public static function details($user_id)
    {
        $query = User::where('id', '=', $user_id);
        //$query = User::getUserColumns($query);
        $query = $query->first();

        return $query;
    }

    public static function isEmailExists($email)
    {
        $obj = User::where('email', $email);
        $obj = $obj->where('active', '!=', 'DELETED');
        $obj = $obj->first();
        return $obj;
    }

    public static function isContactExists($contact, $country_code)
    {
        $obj = User::where('contact', $contact);
        $obj = $obj->where('country_code', '=', $country_code);
        $obj = $obj->where('active', '!=', 'DELETED');
        $obj = $obj->first();
        return $obj;
    }

    public static function isSocialExists($social_id)
    {
        $obj = User::where('acccountIdentifier', $social_id);
        $obj = $obj->where('active', '!=', 'DELETED');
        $obj = $obj->first();
        return $obj;
    }

    public static function updateProfile($request, $id)
    {
        User::where('id', $id)->update([
            'name' => $request->input('name'),
            'handle' => $request->input('handle'),
            'email' => $request->input('email'),
            'aboutus' => $request->input('aboutus'),
            'gender' => $request->input('gender'),
            'dob' => $request->input('dob')
            ]);
    }

    public static function userDetailsWithEmailIdAndPassword($email, $password)
    {

        $user = User::where('email', '=', $email)->first();
        if (!empty($user) && Hash::check($password, $user->password)) 
        {
            return $user;
        }
        return null;
    }

    public static function checkOtp($user_id, $otp)
    {
        $user = DB::table('users')
            ->where('id', '=', $user_id)
            ->where('otp', '=', $otp)->first();

        if (!empty($user)) {
            return $user;
        }
        return null;
    }

    public static function updateNotification($id, $is_notification)
    {
        User::where('id', $id)->update(['is_notification' => $is_notification]);
    }

    public static function updateOtp($id)
    {
        User::where('id', $id)->update([
            'is_otp_verified' => true,
            'otp' => '']);
    }

    public static function setOtp($id, $otp)
    {
        $user = User::where('id', $id)->update([
            'is_otp_verified' => false,
            'otp' => $otp]);

        if (!empty($user)) {
            return $user;
        }
        return null;
    }

    public static function checkOldPassword($id, $password)
    {
        $user = User::where('id', '=', $id)
            ->first();

        if (!empty($user) && Hash::check($password, $user->password)) {

            return $user;
        }
        return null;
    }

    public static function setNewPassword($id, $password)
    {
        $user = User::where('id', $id)->update([
            'password' => Hash::make($password)]);

        if (!empty($user)) {
            return $user;
        }
        return null;
    }

    public static function updateUserImage($user_id, $image_image)
    {
        DB::table('users')
            ->where('id', '=', $user_id)
            ->update(['profile_image' => $image_image]);
    }

    public static function getUserColumns($query)
    {
        $image_url = config('constants.S3_BUCKET_USER_URL');
        return $query->select("users.*", DB::raw("(CASE WHEN users.profile_image !='' THEN CONCAT('$image_url','',users.profile_image)  ELSE '' END) as profile_image"));
    }

    public static function updateForgotPasswordToken($token, $user_id)
    {
        return User::where('id', '=', $user_id)
            ->update(['forgot_password_token' => $token,
                'forgot_password_date' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function isForgotTokenExist($token)
    {
        return User::where('forgot_password_token', '=', $token)->where('active', '!=', 'DELETED')->first();
    }

    public static function changePasword($user_id, $new_password)
    {
        return DB::table('users')
            ->where('id', '=', $user_id)
            ->update(['password' => Hash::make($new_password)
                , 'forgot_password_token' => null
                , 'forgot_password_date' => null
                , 'updated_at' => date('Y-m-d H:i:s')]);
    }

    public static function userSave($request, $id)
    {

        User::where('id', $id)->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'country_code' => $request->input('country_code'),
            'contact' => $request->input('contact')]);
    }

    public static function location($request)
    {
        $user = Auth::user();
        User::where('id', $user->id)->update([
            'is_location' => $request->input('is_location')]);
    }

    public static function updateLocation($request)
    {
        $user = Auth::user();
        User::where('id', $user->id)->update([
            'location' => $request->input('location'),
            'longitude' => $request->input('longitude'),
            'latitude' => $request->input('latitude'),
        ]);
    }

    public static function deleteData($id, $user)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $query = User::where('id', '=', $id)
            ->update(['active' => 'DELETED', 'email' => $current_timestamp . '_' . $user->email]);
        return $query;
    }

    public static function getTotalUserCount($user_type)
    {
        return DB::table('users')->where('active', '!=', 'DELETED')->where('user_type', '=', $user_type)->count();
    }

   

}
