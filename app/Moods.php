<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moods extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'moods';
     protected $fillable = ['id','name','active','createdDate','createdBy'];

	public function parent_detail()
    {
       return $this->hasOne('App\Category', 'id', 'parent_id');
    }




}
