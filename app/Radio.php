<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Radio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'radio';
     public $timestamps = false;
     protected $fillable = ['id','title','description'];


     public static function getUserColumns($query)
     {
         return $query->select("radio.*");
     }

}
