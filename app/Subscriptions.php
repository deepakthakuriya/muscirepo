<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'subscription';

     protected $fillable = ['id','name','price','duration','description','status','createdBy','createdDate'];

    public function subscription_features()
    {
        return $this->hasMany('App\SubscriptionsFeatures','subscriptionId','id');
    }
}
