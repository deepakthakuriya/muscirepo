<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BPMS extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'bpms';

     protected $fillable = ['id','name'];


}
