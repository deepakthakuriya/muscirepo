<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\City;
use App\States;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use DataTables;
use Validator;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = User::latest()->where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $query->where('created_by', Auth()->user()->id);
            }
        })->with('rolesss')->get();

        return view('admin.users.users')->with(['data'=>$data]);
    }


    public function Userdetails(Request $request,$id)
    {
            $data = User::latest()->where(function($query){$query->where('id',$id);})->get();
            return view('admin.users.users')->with(['data'=>$data,'business'=>$business]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::select('id','name')->get();
        $states = States::orderBy('name','ASC')->get();
        return view('admin.users.users-create',compact('roles','states'));
    }
    
    public function CityLength(Request $request)
    {
        $city = City::select('id','city')->where('state_id',$request->state_id)->get();
        return response()->json($city);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:users,email',
            'mobile_no' => 'required|min:10|unique:users,mobile_no',
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required',
            'states_id'=>'required',
            'roles' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
		   return response()->json([
			'status' => false,
			'errors' => $validator->errors()
			]);
        }

        

        $user = new User();
        $user->emp_id = $request->emp_id;
        $user->role_id = $request->roles;
        $user->name = $request->name;

        $user->state = implode(',',$request->states_id);
        $user->city = $request->city;

        $user->email = $request->email;
        $user->mobile_no = $request->mobile_no;

        if($request->hasFile('profile_image'))
        {
            $imageName = time().'.'.$request->profile_image->extension();
            $image_path = public_path('uploads/profile');
            $request->profile_image->move($image_path, $imageName);
            $imageName = "uploads/profile/".$imageName;
            $user->profile_image = $imageName;
        }

        $user->status = $request->status;
        $user->password = Hash::make($request->password);
		$user->created_by = Auth()->user()->id;
        $user->save();
        $user->assignRole($request->roles);

        return response()->json([
            'status' => true,
            'msg' => 'User created successfully'
			]);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = User::find($id);
        return view('admin.users.user-detail',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::select('id','name')->get();
        $userRole = $user->roles->pluck('id','name')->all();
        $states = States::get();
        return view('admin.users.users-edit',compact('user','roles', 'userRole','states'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile_no' => 'required|numeric|digits:10|unique:users,mobile_no,'.$id,
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'states_id'=>'required',
            // 'city'=>'required',
            'roles' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
		   return response()->json([
			'status' => false,
			'errors' => $validator->errors()
			]);
        }

        $user = User::find($id);
        $user->emp_id = $request->emp_id;
        $user->role_id = $request->roles;
        $user->name = $request->name;
       
        $user->email = $request->email;
        $user->mobile_no = $request->mobile_no;
        $user->state = implode(',',$request->states_id);
        $user->city = $request->city;
        $user->password = Hash::make($request->password);
        if($request->hasFile('profile_image'))
        {
            $imageName = time().'.'.$request->profile_image->extension();
            $image_path = public_path('uploads/profile');
            $request->profile_image->move($image_path, $imageName);
            $imageName = "uploads/profile/".$imageName;
            $user->profile_image = $imageName;
        }

        $user->status = $request->status;
		$user->created_by = Auth()->user()->id;
        $user->save();
        if($user->id != 1)
        {
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->assignRole($request->roles);
        }

        return response()->json([
            'status' => true,
            'msg' => 'User updated successfully'
			]);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }



}
