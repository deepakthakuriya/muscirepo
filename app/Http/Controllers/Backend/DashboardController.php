<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Leads;
use App\User;
use App\States;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
          $Totalleads = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->count();

          $Openleads = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->where(['lead_status'=>'Open'])->count();


          $Cancelledleads = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->where(['lead_status'=>'Cancelled'])->count();
            
          $Closedleads = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->where(['lead_status'=>'Closed'])->count();

          $Resheduedleads = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->where(['lead_status'=>'Resheduled'])->count();

        $Contact1 = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->where(['lead_status'=>'1st Contact Esablished'])->count();
            
          $leadsFromFamily = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->where('Lead_from','=','parivaar')->count();

          $leadsFromPrebook = Leads::where(function($query){
            if(!Auth()->user()->hasRole('Super Admin')){
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
                }
            })->where('Lead_from','=','Prebook')->count();
          
          $TOTALLEADS = Leads::count();
          
          
          $leadwithzero = Leads::where(['esclation_status'=>0])->where('lead_status','!=','Closed')->where('lead_status','!=','Cancelled')->count();
          $leadwithone = Leads::where(['esclation_status'=>1])->where('lead_status','!=','Closed')->where('lead_status','!=','Cancelled')->count();
          $leadwithtwo = Leads::where('esclation_status','=',2)->where('lead_status','!=','Closed')->where('lead_status','!=','Cancelled')->count();
          $leadwithmoretwo = Leads::where('esclation_status','>',2)->where('lead_status','!=','Closed')->where('lead_status','!=','Cancelled')->count();
          
        //   echo $leadwithzero;
        //   exit;
            
        //     echo $leadsFromFamily;
        //     echo "<br />";
        //     echo  $leadsFromPrebook;
        // exit;
        $states = States::orderBy('name','ASC')->get();
           
        foreach($states as $key=>$vals)
        {
            
            $states[$key]->total_leads = Leads::where('state',$vals->name)->count();
            $states[$key]->open_leads = Leads::where(['lead_status'=>'Open','state'=>$vals->name])->count();
            $states[$key]->cancelled_leads = Leads::where(['lead_status'=>'Cancelled','state'=>$vals->name])->count();
            $states[$key]->closed_leads = Leads::where(['lead_status'=>'Closed','state'=>$vals->name])->count();
            $states[$key]->rescheduled_leads = Leads::where(['lead_status'=>'Resheduled','state'=>$vals->name])->count();
            $states[$key]->first_leads = Leads::where(['lead_status'=>'1st Contact Esablished','state'=>$vals->name])->count();
        }

         return view('admin.dashboard.dashboard',compact('Totalleads','Contact1','Openleads','Cancelledleads','Closedleads','Resheduedleads','leadsFromFamily','leadsFromPrebook','leadwithzero','leadwithone','leadwithtwo','leadwithmoretwo','TOTALLEADS','states'));
    }
}
