<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use DataTables;
use Validator;
use Auth;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      
        return view('admin.setting.setting');
    }

    public function updatePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'old_password' =>  [
                'required', function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        $fail('Old Password didn\'t match');
                    }
                },
            ],
            'password' => 'required|min:6',
            'confirm_password' => 'required_with:password|same:password|min:6',
        ]);

        if ($validator->fails()) {
		   return response()->json([
			'status' => false,
			'errors' => $validator->errors()
			]);
        }
        
        $user = User::find(Auth::user()->id);
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'status' => true,
            'msg' => 'User Password updated successfully'
			]);


    }
           public function generateNumericOTP($n) {
        $generator = "1@#$%&357902468ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $result = "";
        for ($i = 1; $i <= $n; $i++)
        { 
            $result .= substr($generator, (rand()%(strlen($generator))), 1); 
        } 
          return $result; 
    }
    
    public function ForgotPassword(Request $request){
         $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        
        $validator->errors();

        if($validator->fails()) return redirect()->back()->withErrors(['msg',$validator->errors()]);
    
        $User=User::where(['email'=>$request->email])->first();
        if($User)
        {
            $otp = $this->generateNumericOTP(8);
            
            ini_set("SMTP", "smtp.gmail.com");
    
            ini_set("sendmail_from", "kashifhussain146@gmail.com");
            
            $message='Your New Password is :'.$otp;
            
            $headers = "From: area412@gmail.com";
            
             $mail = $request->email;
            // echo "$mail";
            // exit;
            mail($mail, "Forgot Password", $message, $headers);
    
            $forgot = User::find($User->id);
            $forgot->password = bcrypt($otp);
            $forgot->save();
            // echo "message send on email";
            // exit;
            return redirect()->back()->with(['message'=>'New Password Sent on '.$mail]);
        }
        else  return redirect()->back()->with(['message'=>'E-mail Not Exist in Our Record']);
    }
    




}