<?php

namespace App\Http\Controllers\Api\Mobile\Auth;
use App\ForgotOtp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;
use App\User;
use App\Mail\SendInvoicePDF;
use Mail;

use App\Category;
use App\Genres;
use App\Languages;
use App\Moods;
use App\Pitch;
use App\Tempos;
use App\Themes;
use App\VocalTypes;
use App\Instruments;
use App\BPMS;
use App\MusicGeneres;
use App\UsersGeneres;

use Illuminate\Support\Str;

class CommonController extends Controller
{
    
	public function category(Request $request)
	{
		$category = Category::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($category)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $category
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}
	
	public function genres(Request $request)
	{
		$genres = Genres::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($genres)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $genres
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}
	
	public function languages(Request $request)
	{
		$languages = Languages::select('id','name','type')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($languages)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $languages
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}
	
	public function pitch(Request $request)
	{
		$pitch = Pitch::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($pitch)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $pitch
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}
	
	public function themes(Request $request)
	{
		$themes = Themes::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($themes)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $themes
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}

	public function moods(Request $request)
	{
		$moods = Moods::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($moods)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $moods
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}

	public function vocalTypes(Request $request)
	{
		$vocalTypes = VocalTypes::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($vocalTypes)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $vocalTypes
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}
	
	public function tempos(Request $request)
	{
		$tempos = Tempos::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($tempos)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $tempos
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}

	public function instruments(Request $request)
	{
		$instruments = Instruments::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($instruments)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $instruments
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}

	public function bpms(Request $request)
	{
		$bpms = BPMS::select('id','name')->where('active','Y')->orderBy('id','DESC')->get();
		if(count($bpms)>0)
		{
		    
			return response()->json([
				'status' => true,
				'data' => $bpms
				]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message' => 'No Records Found'
				]);
		}

	}



}
