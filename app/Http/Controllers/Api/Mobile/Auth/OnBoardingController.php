<?php

namespace App\Http\Controllers\Api\Mobile\Auth;
use App\ForgotOtp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;
use App\User;
use App\Mail\SendInvoicePDF;
use Mail;
use Illuminate\Support\Str;

class OnBoardingController extends Controller
{

    public function socialLogin(Request $request)
    {
			$validationarray = 
            [
                'accountType'=>'required|in:EMAIL,GOOGLE,FACEBOOK,INSTAGRAM',
                'acccountIdentifier'=>'required',
            ];
            
            
            $validator = Validator::make($request->all(),$validationarray);

    		if ($validator->fails()) 
    		{
    			$message = [];
    			foreach($validator->errors()->getMessages() as $keys=>$vals)
    			{
    			   foreach($vals as $k=>$v)
    			   {
    				 $message[] =  $v;
    			   }
    			}
    			
    			return response()->json([
    				'status' => false,
    				'message' => $message[0]
    				]);
    		}
            
            
            $user = User::select('id','name','handle','email','aboutus','photo','otp','accountType','acccountIdentifier','gender','dob','isVerified','isSubscribed','active','lastLoggedInDate')->where(['accountType'=>$request->accountType,'acccountIdentifier'=>$request->acccountIdentifier])->first();
            if($user==null)
            {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
				$user->accountType = $request->accountType;
                $user->acccountIdentifier = $request->acccountIdentifier;
                $user->active = 'ACTIVE';
                $user->createdDate = date('Y-m-d H:i:s');
                $user->updatedDate = date('Y-m-d H:i:s');
                $user->lastLoggedInDate = date('Y-m-d H:i:s');
                $user->save();
				
            }
           
			$user->createToken('Personal Access Token')->accessToken;
			$tokenResult = $user->createToken('Personal Access Token');
			$token = $tokenResult->token;
			$token->expires_at = Carbon::now()->addWeeks(12);
			$token->save();

			$addnotification = User::addNotification($user->id);

			return response()->json([
				'status' => true,
				'data' => $user,
				'access_token' => $tokenResult->accessToken,
				'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
				'message'=>'Login Successfully'
			]);
		        
    }

	public function register(Request $request)
    {
			$validationarray = 
            [
                'name'=>'required',
                'handle'=>'nullable|unique:users,handle',
				'email'=>'required|unique:users,email',
				'password'=>'required',
				'confirm_password'=>'required|same:password',
            ];
            
            
            $validator = Validator::make($request->all(),$validationarray);

    		if ($validator->fails()) 
    		{
    			$message = [];
    			foreach($validator->errors()->getMessages() as $keys=>$vals)
    			{
    			   foreach($vals as $k=>$v)
    			   {
    				 $message[] =  $v;
    			   }
    			}
    			
    			return response()->json([
    				'status' => false,
    				'message' => $message[0]
    				]);
    		}

                $user = new User();
                $user->name = $request->name;
                $user->handle = $request->handle;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->aboutus = $request->aboutus;
				$user->active = 'ACTIVE';
                $user->createdDate = date('Y-m-d H:i:s');
                $user->updatedDate = date('Y-m-d H:i:s');
                $user->lastLoggedInDate = date('Y-m-d H:i:s');
                $user->save();
				  

				
 				$user->createToken('Personal Access Token')->accessToken;
				$tokenResult = $user->createToken('Personal Access Token');
				$token = $tokenResult->token;
				$token->expires_at = Carbon::now()->addWeeks(12);
				$token->save();

				$addnotification = User::addNotification($user->id);

				return response()->json([
					'status' => true,
					'data' => $user,
					'access_token' => $tokenResult->accessToken,
					'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
					'message'=>'Registration Successfully'
				]);

	}

	public function signupPhone(Request $request)
	{
			
			$validationarray = ['mobile'=>'required|digits:10|unique:users,acccountIdentifier'];

			$validator = Validator::make($request->all(),$validationarray);

			if ($validator->fails()) 
    		{
    			$message = [];
    			foreach($validator->errors()->getMessages() as $keys=>$vals)
    			{
    			   foreach($vals as $k=>$v)
    			   {
    				 $message[] =  $v;
    			   }
    			}
    			
    			return response()->json([
    				'status' => false,
    				'message' => $message[0]
    				]);
    		}


			$otp = 1234;

			$user = new User();
			$user->accountType = 'PHONE';
			$user->acccountIdentifier = $request->mobile;
			$user->otp = $otp;
			$user->active = 'ACTIVE';
			$user->createdDate = date('Y-m-d H:i:s');
			$user->updatedDate = date('Y-m-d H:i:s');
			$user->lastLoggedInDate = date('Y-m-d H:i:s');
			$user->save();

			return response()->json([
				'status' => true,
				'message'=>'OTP Send Successfully'
			]);
	}


	public function verifyOTP(Request $request)
	{

		$validationarray = [
			'mobile'=>'required|digits:10|exists:users,acccountIdentifier',
			'otp'=>'required',	
		];

		$validator = Validator::make($request->all(),$validationarray);
		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

		$user = User::select('id','name','handle','email','aboutus','photo','otp','accountType','acccountIdentifier','gender','dob','isVerified','isSubscribed','active','lastLoggedInDate')->where(['acccountIdentifier'=>$request->mobile,'otp'=>$request->otp])->first();
		if($user)
		{
			$user->createToken('Personal Access Token')->accessToken;
			$tokenResult = $user->createToken('Personal Access Token');
			$token = $tokenResult->token;
			$token->expires_at = Carbon::now()->addWeeks(12);
			$token->save();

			$addnotification = User::addNotification($user->id);

			return response()->json([
				'status' => true,
				'data' => $user,
				'access_token' => $tokenResult->accessToken,
				'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
				'message'=>'Registration Successfully'
			]);
		}
		else
		{
			return response()->json([
				'status' => false,
				'message'=>'OTP Not Matched'
			]);
		}

	}


	public function resendOTP(Request $request)
	{

		$validationarray = ['mobile'=>'required|digits:10|exists:users,acccountIdentifier'];

		$validator = Validator::make($request->all(),$validationarray);
		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

		$user = User::where('acccountIdentifier',$request->mobile)->first();
		$user->otp = 4321;
		$user->save();


		return response()->json([
			'status' => true,
			'message'=>'OTP Re-send Successfully'
		]);
	}

    public function login(Request $request)
    {
		$input = $request->post('input');
		$pattern = "/@handle.com/";

		if(preg_match($pattern, $input))
		{
			$cred['handle']=$request->input;
			$validation['input']='required|email|exists:users,handle';
			$validation['password']='required';
		}
		else
		{
			$cred['email']=$request->input;
			$validation['input']='required|email|exists:users,email';
			$validation['password']='required';
		}

		$validator = Validator::make($request->all(), $validation);
		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

		$user = User::select('id','name','handle','email','password','aboutus','photo','otp','accountType','acccountIdentifier','gender','dob','isVerified','isSubscribed','active','lastLoggedInDate')->where($cred)->first();
		if($user)
		{
			if (Hash::check($request->password,$user->password))
			{
				$user->createToken('Personal Access Token')->accessToken;
				$tokenResult = $user->createToken('Personal Access Token');
				$token = $tokenResult->token;
				$token->expires_at = Carbon::now()->addWeeks(12);
				$token->save();

				return response()->json([
				'status' => true,
				'data' => $user,
				'access_token' => $tokenResult->accessToken,
				'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
				'message'=>'Login Successfully'
				]);
			}
			else
			{
				return response()->json(['status'=>false,'message'=>'Invalid Password']);
			}
		}
		else
		{
				return response()->json(['status'=>false,'message'=>'Unauthorized User']);               
		}
    }


}
