<?php

namespace App\Http\Controllers\Api\Mobile\Auth;
use Validator;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\UserFollowers;
use App\NotificationSettings;
use App\Music;
use App\Offers;
use App\User;
use App\MusicLikes;
use App\Radio;
use App\Subscriptions;
use App\SubscriptionsFeatures;

class MusicController extends Controller
{

	public function getProfile(Request $request)
	{
		$user = Auth::user();
		$userDetails = User::select('id','name','handle','email','aboutus','photo','accountType','acccountIdentifier','gender','dob','isVerified','isSubscribed','active','lastLoggedInDate')->find($user->id);
		return response()->json(['status'=>true,'data'=>$userDetails], 200);
	}

	public function likesMusic(Request $request)
	{
		$user = Auth::user();

		$validator = Validator::make($request->all(), [
            'music_id' => 'required',
        ]);

		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

		$check = MusicLikes::where(['musicId'=>$request->music_id,'likedBy'=>$user->id])->first();
		if($check==null)
		{
			$musiclikes = new MusicLikes();
			$musiclikes->musicId = $request->music_id;
			$musiclikes->likedBy = $user->id;
			$musiclikes->createdDate = date('Y-m-d H:i:s');
			$musiclikes->save();
			$message = 'Music Liked';
		}
		else
		{
			MusicLikes::where(['musicId'=>$request->music_id,'likedBy'=>$user->id])->delete();
			$message = 'Music UnLiked';
		}

		return response()->json(['message' => $message, 'status' => true]);
	}

	public function getLikedMusic(Request $request)
	{
		$user = Auth::user();
		$likedMusic = MusicLikes::where('likedBy',$user->id)->orderBy('id','DESC')->paginate(10);
		return response()->json(['message' => 'Music Liked List', 'status' => true,'data'=>$likedMusic]);
	}

	public function updateProfile(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'handle' => 'required',
            'email' => 'required',
            'aboutus' => 'required',
            'gender' => 'required',
            'dob' => 'required',
        ]);

		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

        $email_exist = User::isEmailExists($request->input('email'));

        if (!empty($email_exist)) 
		{
            if ($email_exist->id != $user->id) 
			{
                return response()->json(['status' => false, 'message' => 'E-mail ID Already Exists'], 200);
            }
        }

        User::updateProfile($request, $user->id);
        $UserDetails =  User::select('id','name','handle','email','aboutus','photo','accountType','acccountIdentifier','gender','dob','isVerified','isSubscribed','active','lastLoggedInDate')->find($user->id);
        return response()->json(['message' => 'Successfully Saved', 'status' => true, 'result' => $UserDetails]);
    }

	public function followUser(Request $request)
	{
		$user = Auth::user();

		$validator = Validator::make($request->all(), [
            'followed_by' => 'required|exists:users,id',
        ]);

		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

		$check = UserFollowers::where(['userId'=>$user->id,'followedBy'=>$request->followed_by])->first();
		if($check==null)
		{
			
			$userfollowers = new UserFollowers();
			$userfollowers->userId = $user->id;
			$userfollowers->followedBy = $request->followed_by;
			$userfollowers->save();

			$message = 'Following Successfully';

		}
		else
		{
			UserFollowers::where(['userId'=>$user->id,'followedBy'=>$request->followed_by])->delete();
			$message = 'UnFollowing Successfully';
		}

		return response()->json(['status' => true, 'message' => $message ], 200);

	}

	public function followingLists(Request $request)
	{
		$user = Auth::user();
		$followers = UserFollowers::where('followedBy',$user->id)
									->join('users', 'users.id', '=', 'userfollowers.followedBy')
									->select('users.id','users.name', 'users.handle','users.photo','users.email')
									->orderBy('id','DESC')
									->get();

		return response()->json(['status' => true, 'message' => 'following list','data'=>$followers ], 200);								
	}

	public function followerLists(Request $request)
	{
		$user = Auth::user();
		$followers = UserFollowers::where('userId',$user->id)
									->join('users', 'users.id', '=', 'userfollowers.followedBy')
									->select('users.id','users.name', 'users.handle','users.photo','users.email')
									->orderBy('id','DESC')
									->get();

		return response()->json(['status' => true, 'message' => 'follower list','data'=>$followers ], 200);								
	}


	public function getNotification(Request $request)
	{
		$user = Auth::user();
		$notification = NotificationSettings::where('userId',$user->id)->first();
		if($notification)
		{
			return response()->json(['status' => true, 'message' => 'Notification Setting','data'=>$notification ], 200);	
		}
		else
		{
			return response()->json(['status' => false, 'message' => 'No Notification Setting Set'], 200);	
		}
	}


	public function updateNotification(Request $request)
	{
		$user = Auth::user();
		$validator = Validator::make($request->all(), [
            'push' => 'nullable|in:Y,N',
            'auto_play' => 'nullable|in:Y,N',
            'likes_on_tune' => 'nullable|in:Y,N',
            'new_followers' => 'nullable|in:Y,N',
            'new_features' => 'nullable|in:Y,N',
            'suggested_content' => 'nullable|in:Y,N',
            'new_promotions' => 'nullable|in:Y,N',
        ]);

		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

		$notification = NotificationSettings::where('userId',$user->id)->first();
		$notification->userId = $user->id;
		$notification->push = $request->push;
		$notification->autoPlay = $request->auto_play;
		$notification->likesOnTune = $request->likes_on_tune;
		$notification->newFollowers = $request->new_followers;
		$notification->newFeatures = $request->new_features;
		$notification->suggestedContent = $request->suggested_content;
		$notification->newPromotions = $request->new_promotions;
		$notification->save();

		return response()->json(['status' => true, 'message' => 'Notification Setting Updated','data'=>$notification ], 200);
	}

    public function changePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string',
            'new_password' => 'required|string',
        ]);

		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}

        $user = Auth::user();
        $user_id = $user->id;
        $old_password = User::checkOldPassword($user_id, $request->input('old_password'));

        if (!empty($old_password)) {
            $userDetails = User::setNewPassword($user_id, $request->input('new_password'));
            if (!empty($userDetails)) {
                return response()->json(array('status' => trans('messages.SUCCESS'), 'message' => trans('messages.SUCCESSFULLY_CHANGED_PASSWORD')), 200);
                exit();
            } else {
                return response()->json(array('status' => trans('messages.ERROR'), 'message' => trans('messages.PASSWORD_FAILED')), 403);
                exit();
            }

        } else {
            return response()->json(array('status' => trans('messages.ERROR'), 'message' => trans('messages.WRONG_PASSWORD')), 403);
            exit();
        }

    }

    public function uploadImage(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $destinationPath = config('constants.S3_BUCKET_FOLDER_USER');
        if ($request->hasFile('image')) 
		{
            $user_image = $request->file('image');
            $milliseconds = round(microtime(true) * 1000);
            $user_image_name = "IMG_" . $milliseconds . "_" . $user_image->getClientOriginalName();

            $thumbnailiconupload = Helper::fileUpload($user_image, $destinationPath, $user_image_name);

            if ($thumbnailiconupload != false) {
                User::updateUserImage($user_id, $user_image_name);
                $user_url = config('constants.S3_BUCKET_USER_URL') . $user_image_name;
                return response()->json(['status' => trans('messages.SUCCESS'), 'message' => trans('messages.SUCCESSFULLY_UPLOADED'), 'result' => $user_url]);
            } else {
                return response()->json(['status' => trans('messages.ERROR'), 'message' => trans('messages.SOMETHING_WENT_WRONG')], 403);

            }

        }
        return response()->json(['status' => trans('messages.ERROR'), 'message' => trans('messages.SOMETHING_WENT_WRONG')], 403);
    }

	public function choosegeneres(Request $request)
	{
		$user = Auth::user();

		$validationarray = [
			'userId'=>'required|exists:users,id',
			'genreId'=>'required',
			'genreId.*'=>'required|exists:genres,id',	
		];

		$validator = Validator::make($request->all(),$validationarray);
		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}


		foreach($request->genreId as $key=>$vals)
		{
			$musicgeneres = New UsersGeneres();
			$musicgeneres->userId = $user->id;
			$musicgeneres->genreId = $vals;
			$musicgeneres->save();
		}

		
		return response()->json([
			'status' => true,
			'message' => 'Generes Save Successfully'
			]);

	}

	public function searchMusic(Request $request)
	{

		$post = $request->post();

		$music = Music::where(function($query) use ($post){
					
					if(is_array($post['language_id']) && count($post['language_id'])>0)
					{
						$query->whereIn('languageId',$post['language_id']);
					}

					if(is_array($post['pitch_id']) && count($post['pitch_id'])>0)
					{
						$query->whereIn('pitchId',$post['pitch_id']);
					}

					if(is_array($post['temp_id']) && count($post['temp_id'])>0)
					{
						$query->whereIn('tempoId',$post['temp_id']);
					}

			})->orderBy('id','DESC')->paginate(10);
		
		if(count($music)>0)
		{
			return response()->json(['status'=>true,'message'=>'Search List','data'=>$music],200);
		}
		else
		{
			return response()->json(['status'=>false,'message'=>'No Records Found'],200);			
		}
	}


	public function sendOffer(Request $request)
	{

		$user = Auth::user();

		$validationarray = [
			'music_id'=>'required|exists:musics,id',
			'price'=>'required|numeric',
			'revised_rice'=>'nullable|numeric'
		];

		$validator = Validator::make($request->all(),$validationarray);
		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}
		
		$offers = new Offers();
		$offers->userId = $user->id;
		$offers->musicId = $request->music_id;
		$offers->price = $request->price;
		$offers->status = $request->status;		
		$offers->message = $request->message;
		$offers->revisedPrice = $request->revised_rice;
		$offers->createdDate = date('Y-m-d H:i:s');
		$offers->save();

		return response()->json(['status'=>true,'message'=>'Offer Send Successfully'],200);

	}

	public function getTrending(Request $request)
	{

		$topDay = Music::join('musicliked','musics.id','musicliked.musicId')
						->select('musics.id','musics.artistId','musics.title','musics.description','musics.likedCount','musics.languageId','musics.moodId','musics.pitchId','musics.tempoId','musics.vocalTypeId','musics.bpmId','musics.categoryId','musics.priceFrom','musics.priceTo','musics.status','musicliked.createdDate')
						->whereDate('musicliked.createdDate','=',date('Y-m-d'))
						->where('status','=','PUBLISHED')
						->orderBy('id','DESC')
						->paginate(10);

		$topMonth = Music::join('musicliked','musics.id','musicliked.musicId')
						->select('musics.id','musics.artistId','musics.title','musics.description','musics.likedCount','musics.languageId','musics.moodId','musics.pitchId','musics.tempoId','musics.vocalTypeId','musics.bpmId','musics.categoryId','musics.priceFrom','musics.priceTo','musics.status','musicliked.createdDate')
						->whereMonth('musicliked.createdDate','=',date('m'))
						->whereYear('musicliked.createdDate','=',date('Y'))
						->where('status','=','PUBLISHED')
						->orderBy('id','DESC')
						->paginate(10);

		$topYear = Music::join('musicliked','musics.id','musicliked.musicId')
						->select('musics.id','musics.artistId','musics.title','musics.description','musics.likedCount','musics.languageId','musics.moodId','musics.pitchId','musics.tempoId','musics.vocalTypeId','musics.bpmId','musics.categoryId','musics.priceFrom','musics.priceTo','musics.status','musicliked.createdDate')
						->whereYear('musicliked.createdDate','=',date('Y'))
						->where('status','=','PUBLISHED')
						->orderBy('id','DESC')
						->paginate(10);


		return response()->json(['status'=>true,'topDay'=>$topDay,'topMonth'=>$topMonth,'topYear'=>$topYear],200);
	}



	public function listTrending(Request $request)
	{
		$post = $request->post();
		
		$topDay = Music::join('musicliked','musics.id','musicliked.musicId')
						->select('musics.id','musics.artistId','musics.title','musics.description','musics.likedCount','musics.languageId','musics.moodId','musics.pitchId','musics.tempoId','musics.vocalTypeId','musics.bpmId','musics.categoryId','musics.priceFrom','musics.priceTo','musics.status','musicliked.createdDate')
						->where(function($query) use ($post) {
							
							if($post['type']=='Day')
							{
								$query->whereDate('musicliked.createdDate','=',date('Y-m-d'));
							}

							if($post['type']=='Month')
							{
								$query->whereMonth('musicliked.createdDate','=',date('m'))->whereYear('musicliked.createdDate','=',date('Y'));
							}

							if($post['type']=='Year')
							{
								$query->whereYear('musicliked.createdDate','=',date('Y'));
							}
						})
						->orderBy('id','DESC')
						->where('status','=','PUBLISHED')
						->paginate(10);

		return response()->json(['status'=>true,'data'=>$topDay],200);
	}

	public function getRadio(Request $request)
	{
		$validationarray = [
			'language_id'=>'required|numeric|exists:languages,id',
			'genere_id'=>'required|numeric|exists:genres,id',
		];

		$validator = Validator::make($request->all(),$validationarray);
		if ($validator->fails()) 
		{
			$message = [];
			foreach($validator->errors()->getMessages() as $keys=>$vals)
			{
				foreach($vals as $k=>$v)
				{
					$message[] =  $v;
				}
			}
			
			return response()->json([
				'status' => false,
				'message' => $message[0]
				]);
		}


		$radio = new Radio();
		$radio->languageId = $request->language_id;
		$radio->genreId = $request->genere_id;
		$radio->createdDate = date('Y-m-d H:i:s');
		$radio->save();


		return response()->json(['status'=>true,'message'=>'Radio Save'],200);


	}


	public function getSubscriptions(Request $request)
	{
		$subscription = Subscriptions::with('subscription_features')->where('status','PUBLISHED')->get();
		if(count($subscription)>0)
		{
			return response()->json(['status'=>true,'message'=>'Subscription List','data'=>$subscription],200);
		}
		else
		{
			return response()->json(['status'=>false,'message'=>'No Records Found'],200);			
		}

	}





}
