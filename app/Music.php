<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'musics';
     public $timestamps = false;
     protected $fillable = ['id','title','description'];


     public static function getUserColumns($query)
     {
         return $query->select("musics.*");
     }

}
