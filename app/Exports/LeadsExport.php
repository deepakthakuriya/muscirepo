<?php
  
namespace App\Exports;
  
use App\Leads;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
  use Maatwebsite\Excel\Concerns\ShouldAutoSize;
class LeadsExport implements FromCollection,ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(isset($_POST['todate']) && !empty($_POST['todate']) && isset($_POST['fromdate']) && !empty($_POST['fromdate']))
        {
            return Leads::select('id',
            'name',
            'email',
            'mobile',
            'state',
            'city',
            'category',
            'company',
            'model',
            'battery_type',
            'warranty',
            'mrp',
            'capacity',
            'group_company',
            'how_do_you_know',
            'Lead_from',
            'lead_status',
            'comment',
            'esclation_status',
            'created_at',
            'closed_at',
            'turn_around',
            'contact_at')->where(function($query){
                            if(!Auth()->user()->hasRole('Super Admin'))
                            {
                                $user = User::where(['id'=>auth()->user()->id])->first();
                                $query->whereIn('state',explode(',',$user->state));
                            }
                        })
                        ->where('created_at','>=',$_POST['todate'])
                        ->where('created_at','<=',$_POST['fromdate'])
                        ->get();
        }
        else
        {
          $totalleads =  Leads::select('id',
            'name',
            'email',
            'mobile',
            'state',
            'city',
            'category',
            'company',
            'model',
            'battery_type',
            'warranty',
            'mrp',
            'capacity',
            'group_company',
            'how_do_you_know',
            'Lead_from',
            'lead_status',
            'comment',
           'esclation_status',
            'created_at',
            'closed_at',
            'turn_around',
            'contact_at')->where(function($query){
            if(!Auth()->user()->hasRole('Super Admin'))
            {
                $user = User::where(['id'=>auth()->user()->id])->first();
                $query->whereIn('state',explode(',',$user->state));
            }
        })->get();
        return $totalleads;
            //return Leads::get();
        }
    }
    
     public function headings(): array
    {
        return [
            'id',
            'name',
            'email',
            'mobile',
            'state',
            'city',
            'category',
            'company',
            'model',
            'battery_type',
            'warranty',
            'mrp',
            'capacity',
            'group_company',
            'how_do_you_know',
            'Lead_from',
            'lead_status',
            'comment',
           'esclation_status',
            'created_at',
            'closed_at',
            'turn_around',
            'contact_at'
        ];
    }
    
}