<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionsFeatures extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'subscriptionsfeatures';

     protected $fillable = ['id','subscriptionId','feature','count'];

}
