<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicLikes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'musicliked';
     public $timestamps = false;
     protected $fillable = ['id','musicId','likedBy'];


}
