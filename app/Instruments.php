<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instruments extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'instruments';

     protected $fillable = ['id','name'];


}
