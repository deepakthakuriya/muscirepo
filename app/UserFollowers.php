<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFollowers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
	 protected $table = 'userfollowers';
     protected $fillable = ['id','userId','followedBy'];


}
