<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersGeneres extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'usergenres';
     public $timestamps = false;
     protected $fillable = ['id','userId','genreId'];

}
