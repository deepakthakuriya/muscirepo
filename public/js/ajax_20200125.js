$('[data-toggle="tooltip"]').tooltip();
var ___GroupAndServices = '';
var __SubServices = '';
var __Faults = '';
var __AdditionalFaults = '';
var __Discounts = '';
var __AFFlag = false;

function ___getAssignedServicesByCity() {
    var parameters = 'city_id=' + $('#city_id option:selected').val();
    $.ajax({
        type: 'get',
        url: '/api/get-citywiseservandsubserv',
        data: parameters,
        dataType: 'json',
        success: function (data) {
            $('#city').val($('#city_id option:selected').text());
            ___GroupAndServices = data['GroupAndServices'];
            __SubServices = data['SubServices'];
            __Faults = data['Faults'];
            __Discounts = data['Discounts'];
            ___LoadGroupAndServices();
        }
    });
}

function getReasonBydId(id) {
    var parameter = 'ReasonId=' + id;
    $("#approve_close").click(function () {
        $('#approve_' + id).prop("checked", false);
    });
    $.ajax({
        type: 'get',
        url: '/api/getReasonData',
        data: parameter,
        success: function (data) {
            $("#approveText").html(data);
            $("#approveReason").html('Approve Reason');
        }
    });
}

function updateCity(id) {
    var status = 2;
    if ($("#checkId" + id).is(':checked')) {
        status = 1;
    }
    var parameter = 'active_city=' + status;
    parameter += '&updateId=' + id;
    $.ajax({
        type: 'post',
        url: '/api/getCityUpdate',
        data: parameter,
        success: function () {}
    });
}


function ___getAssignedServicesByCityOld() {
    var parameters = 'city_id=' + $('#city_id option:selected').val();
    $.ajax({
        type: 'get',
        url: '/api/get_assigned_service_by_city',
        data: parameters,
        dataType: 'json',
        success: function (data) {}
    });
}

function ___getAssignedData() {
    var parameters = 'city_id=' + $('#city_id option:selected').val();
    $.ajax({
        type: 'get',
        url: '/api/get_assigned_data',
        data: parameters,
        dataType: 'json',
        success: function (data) {
            for (var serv in data) {
                $('.Serv' + serv).attr('checked', 'checked');
                for (var subserv in data[serv]) {
                    $('.SubServ' + serv + subserv).attr('checked', 'checked');
                    for (var nowtext in data[serv][subserv]) {
                        if (nowtext == 'multi_group') {
                            $('.MultiGroup' + serv + subserv).attr('checked', 'checked');
                        } else {
                            for (var InBookWork in data[serv][subserv][nowtext]) {
                                if (InBookWork == 'in_booking' && data[serv][subserv][nowtext][InBookWork] == 1) {
                                    $('.InBooking' + serv + subserv + nowtext).attr('checked', 'checked');
                                }
                                if (InBookWork == 'in_working' && data[serv][subserv][nowtext][InBookWork] == 1) {
                                    $('.InWorking' + serv + subserv + nowtext).attr('checked', 'checked');
                                }
                            }
                        }
                    }
                }
            }
        }
    });
}

function ___AddHourRateDiscount(thisObj, serv, subserv, fault) {
    var trId = (serv + subserv + fault).replace(/ /g, '-');
    var preDataIndes = parseInt($(thisObj).attr('data-index'));
    var dataIndex = parseInt($(thisObj).attr('data-index')) + 1;
    var qtyVal = $('.slTo' + preDataIndes + trId).val();
    var discountVal = $('#Dis' + preDataIndes + trId).val();
    if (qtyVal >= 10) {
        alert(serv + ' ' + subserv + ' ' + fault + ' already added hour limit is 10. you can not add more option.');
        return false;
    }
    if (qtyVal == '' || discountVal == '') {
        alert('Please Select Hour To & Discount Value!');
        return true;
    } else {
        $(thisObj).attr('data-index', dataIndex);
        var ServRowSpan = $('.Serv' + serv.replace(/ /g, '-')).attr('rowspan');
        var SubServRowSpan = $('.SubServ' + (serv + subserv).replace(/ /g, '-')).attr('rowspan');
        var htmlRowData = '<tr id="TRRowId' + dataIndex + trId + '">';
        htmlRowData += '<td class="align-top">';
        htmlRowData += '<select class="form-control form-control-sm sltFromAjax slFrom' + dataIndex + trId + '" name="datas[' + serv + '][' + subserv + '][' + fault + '][QtyFrom][' + dataIndex + ']">';
        htmlRowData += '<option value="' + (parseInt(qtyVal) + 1) + '">' + (parseInt(qtyVal) + 1) + '</option>';
        htmlRowData += '</select>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-top">';
        htmlRowData += '<select class="form-control form-control-sm sltToAjax slTo' + dataIndex + trId + '" name="datas[' + serv + '][' + subserv + '][' + fault + '][QtyTo][' + dataIndex + ']">';
        htmlRowData += '<option value="">Select</option>';
        for (var i = (parseInt(qtyVal) + 1); i <= 10; i++) {
            htmlRowData += '<option value="' + i + '">' + i + '</option>';
        }
        var OnKeyUpFun = 'onkeyup="___calcDiscountAmount(this, \'' + serv + '\', \'' + subserv + '\', \'' + fault + '\', ' + dataIndex + ');"';
        htmlRowData += '</select>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-top">';
        htmlRowData += '<input type="text" value="" class="form-control form-control-sm text-right" name="datas[' + serv + '][' + subserv + '][' + fault + '][Discount][' + dataIndex + ']" id="Dis' + dataIndex + trId + '" ' + OnKeyUpFun + '/>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-middle">';
        htmlRowData += '<input readonly="readonly" type="text" value="" class="form-control form-control-sm text-right gtAmount' + dataIndex + trId + '"  name="datas[' + serv + '][' + subserv + '][' + fault + '][Amount][' + dataIndex + ']"/>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-top">';
        var serviceName = serv.replace(' ', '-');
        var subServiceName = subserv.replace(' ', '-');
        var faultName = fault.replace(' ', '-');
        htmlRowData += '<input type="text" value="" class="' + serviceName + subServiceName + faultName + 'Points' + dataIndex + ' form-control form-control-sm text-right" name="datas[' + serv + '][' + subserv + '][' + fault + '][Points][' + dataIndex + ']" />';
        htmlRowData += '</td>';
        htmlRowData += '<td class="text-center">';
        htmlRowData += '<button type="button" class="btn btn-danger btn-xs" onclick="___RemoveRateDiscount(\'' + trId + '\', \'' + serv + '\', \'' + subserv + '\', \'' + dataIndex + '\', \'' + preDataIndes + '\');"><i class="fas fa-minus"></i></button>';
        htmlRowData += '</td>';
        htmlRowData += '</tr>';
        // $('.SubServFault'+trId).attr('rowspan', (parseInt($('.SubServFault'+trId).attr('rowspan'))+1));
        //$('#TRRowId'+preDataIndes+trId).after(htmlRowData);

        $('.Serv' + serv.replace(/ /g, '-')).attr('rowspan', (parseInt(ServRowSpan) + 1));
        $('.SubServ' + (serv + subserv).replace(/ /g, '-')).attr('rowspan', (parseInt(SubServRowSpan) + 1));
        $('.SubServFault' + trId).attr('rowspan', (parseInt($('.SubServFault' + trId).attr('rowspan')) + 1));
        $('#TRRowId' + preDataIndes + trId).after(htmlRowData);
    }
}

function ___AddRateDiscount(thisObj, serv, subserv, fault) {
    var trId = (serv + subserv + fault).replace(/ /g, '-');
    var preDataIndes = parseInt($(thisObj).attr('data-index'));
    //alert(preDataIndes);
    var dataIndex = parseInt($(thisObj).attr('data-index')) + 1;
    var qtyVal = $('.slTo' + preDataIndes + trId).val();
    var discountVal = $('#Dis' + preDataIndes + trId).val();
    if (qtyVal >= 10) {
        alert(serv + ' ' + subserv + ' ' + fault + ' already added qty limit is 10. you can not add more option.');
        return false;
    }
    if (qtyVal == '' || discountVal == '') {
        alert('Please Select Qty To & Discount Value!');
        return true;
    } else {
        //$('.slFrom' + preDataIndes + trId).attr('disabled', 'disabled');
        //$('.slTo' + preDataIndes + trId).attr('disabled', 'disabled');
        $(thisObj).attr('data-index', dataIndex);
        var ServRowSpan = $('.Serv' + serv.replace(/ /g, '-')).attr('rowspan');
        var SubServRowSpan = $('.SubServ' + (serv + subserv).replace(/ /g, '-')).attr('rowspan');
        var htmlRowData = '<tr id="TRRowId' + dataIndex + trId + '">';
        htmlRowData += '<td class="align-top">';
        htmlRowData += '<select class="form-control form-control-sm sltFromAjax slFrom' + dataIndex + trId + '" name="datas[' + serv + '][' + subserv + '][' + fault + '][QtyFrom][' + dataIndex + ']">';
        htmlRowData += '<option value="' + (parseInt(qtyVal) + 1) + '">' + (parseInt(qtyVal) + 1) + '</option>';
        htmlRowData += '</select>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-top">';
        htmlRowData += '<select class="form-control form-control-sm sltToAjax slTo' + dataIndex + trId + '" name="datas[' + serv + '][' + subserv + '][' + fault + '][QtyTo][' + dataIndex + ']">';
        htmlRowData += '<option value="">Select</option>';
        for (var i = (parseInt(qtyVal) + 1); i <= 10; i++) {
            htmlRowData += '<option value="' + i + '">' + i + '</option>';
        }
        var OnKeyUpFun = 'onkeyup="___calcDiscountAmount(this, \'' + serv + '\', \'' + subserv + '\', \'' + fault + '\', ' + dataIndex + ');"';

        htmlRowData += '</select>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-top">';
        htmlRowData += '<input type="text" value="" class="form-control form-control-sm text-right" name="datas[' + serv + '][' + subserv + '][' + fault + '][Discount][' + dataIndex + ']" id="Dis' + dataIndex + trId + '" ' + OnKeyUpFun + '/>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-middle">';
        htmlRowData += '<input readonly="readonly" type="text" value="" class="form-control form-control-sm text-right gtAmount' + dataIndex + trId + '"  name="datas[' + serv + '][' + subserv + '][' + fault + '][Amount][' + dataIndex + ']"/>';
        htmlRowData += '</td>';
        htmlRowData += '<td class="align-top">';
        var serviceName = serv.replace(' ', '-');
        var subServiceName = subserv.replace(' ', '-');
        var faultName = fault.replace(' ', '-');


        htmlRowData += '<input type="text" value="" class="' + serviceName + subServiceName + faultName + 'Points' + dataIndex + ' form-control form-control-sm text-right" name="datas[' + serv + '][' + subserv + '][' + fault + '][Points][' + dataIndex + ']" />';
        htmlRowData += '</td>';
        htmlRowData += '<td class="text-center">';
        htmlRowData += '<button type="button" class="btn btn-danger btn-xs" onclick="___RemoveRateDiscount(\'' + trId + '\', \'' + serv + '\', \'' + subserv + '\', \'' + dataIndex + '\', \'' + preDataIndes + '\');"><i class="fas fa-minus"></i></button>';
        htmlRowData += '</td>';
        htmlRowData += '</tr>';
        $('.Serv' + serv.replace(/ /g, '-')).attr('rowspan', (parseInt(ServRowSpan) + 1));
        $('.SubServ' + (serv + subserv).replace(/ /g, '-')).attr('rowspan', (parseInt(SubServRowSpan) + 1));
        $('.SubServFault' + trId).attr('rowspan', (parseInt($('.SubServFault' + trId).attr('rowspan')) + 1));
        $('#TRRowId' + preDataIndes + trId).after(htmlRowData);
    }
}

function ___RemoveRateDiscount(trId, serv, subserv, dataIndex, preDataIndes) {
    $('#TRRowId' + dataIndex + trId).remove();
    $('.Serv' + serv.replace(/ /g, '-')).attr('rowspan', (parseInt($('.Serv' + serv).attr('rowspan')) - 1));
    $('.SubServ' + (serv + subserv).replace(/ /g, '-')).attr('rowspan', (parseInt($('.SubServ' + (serv + subserv)).attr('rowspan')) - 1));
    $('.SubServFault' + trId).attr('rowspan', (parseInt($('.SubServFault' + trId).attr('rowspan')) - 1));
    $('.btnPlus' + trId).attr('data-index', (parseInt($('.btnPlus' + trId).attr('data-index')) - 1));
    $('.slFrom' + preDataIndes + trId).removeAttr('disabled');
    $('.slTo' + preDataIndes + trId).removeAttr('disabled');
}

function ___AddAdditionalOfferRateDiscount() {
    var htmlData = '';
}

function ___getCityWiseRateData(tbl_name) {
    if (tbl_name == 'assign_service_cities') {
        var parameters = 'city_id=' + $('#city_id option:selected').val();
        $('.sltFrom').attr('disabled', 'disabled');
        $('.sltTo').attr('disabled', 'disabled');
        $('.DiscountBox').attr('disabled', 'disabled');
        $('.RateBox').attr('disabled', 'disabled');
        $('.PointsBox').attr('disabled', 'disabled');
        $('.btnPlusBox').attr('disabled', 'disabled');
        $.ajax({
            type: 'get',
            url: '/api/assign_service_cities',
            data: parameters,
            dataType: 'json',
            success: function (data) {
                for (serv in data) {
                    for (subserv in data[serv]) {
                        for (fault in data[serv][subserv]) {
                            $('.slFrom1' + (serv + subserv + fault).replace(/ /g, '-')).removeAttr('disabled');
                            $('.slTo1' + (serv + subserv + fault).replace(/ /g, '-')).removeAttr('disabled');
                            $('#Rate1' + (serv + subserv + fault).replace(/ /g, '-')).removeAttr('disabled');
                            $('#Dis1' + (serv + subserv + fault).replace(/ /g, '-')).removeAttr('disabled');
                            $('.' + (serv + subserv + fault).replace(/ /g, '-') + 'Points1').removeAttr('disabled');
                            $('.btnPlus' + (serv + subserv + fault).replace(/ /g, '-')).removeAttr('disabled');
                        }
                    }
                }
            }
        });
    } else {
        var parameters = 'city_id=' + $('#assign_city_id option:selected').val();
        $('.sltFrom').attr('disabled', 'disabled');
        $('.sltTo').attr('disabled', 'disabled');
        $('.DiscountBox').attr('disabled', 'disabled');
        $('.RateBox').attr('disabled', 'disabled');
        $('.PointsBox').attr('disabled', 'disabled');

        var urlToProcess = '';
        if (tbl_name == 'service_city_hour_rates') {
            urlToProcess = '/api/get_citywisehourrate_data';
        } else {
            urlToProcess = '/api/get_citywiserate_data';
        }
        $.ajax({
            type: 'get',
            url: urlToProcess,
            data: parameters,
            dataType: 'json',
            success: function (data) {
                for (serv in data) {
                    for (subserv in data[serv]) {
                        for (fault in data[serv][subserv]) {
                            var counter = data[serv][subserv][fault]['counter'];
                            var faultrate = d = data[serv][subserv][fault]['faultrate'];
                            $('#Rate1' + serv + subserv + fault).val(faultrate);
                            var i = 1;
                            for (qty in data[serv][subserv][fault]['data']) {
                                var qtyArr = qty.split('-');
                                var qtyFrom = qtyArr[0];
                                var qtyTo = qtyArr[1];
                                var discount = data[serv][subserv][fault]['data'][qty]['discount'];
                                var amount = data[serv][subserv][fault]['data'][qty]['amount'];
                                var points = data[serv][subserv][fault]['data'][qty]['points'];
                                $('.slFrom' + i + serv + subserv + fault).val(qtyFrom);
                                $('.slTo' + i + serv + subserv + fault).val(qtyTo);
                                $('#Dis' + i + serv + subserv + fault).val(discount);
                                $('.gtAmount' + i + serv + subserv + fault).val(amount);
                                $('.' + serv + subserv + fault + 'Points' + i).val(points);
                                if (i < counter) $('.btnPlus' + serv + subserv + fault).trigger('click');
                                i++;
                            }
                        }
                    }
                }
            }
        });
    }
}

function ___getCityWiseHourRateData() {
    var parameters = 'city_id=' + $('#assign_city_id option:selected').val();
    $.ajax({
        type: 'get',
        url: '/api/get_citywiserate_data',
        data: parameters,
        dataType: 'json',
        success: function (data) {

        }
    });
}

function ___getCities() {
    var parameters = 'state_id=' + $('#state_id option:selected').val();
    $.ajax({
        type: 'get',
        url: '/api/getCities',
        data: parameters,
        dataType: 'json',
        success: function (data) {
            $('#state').val($('#state_id option:selected').text());
            $('#city_id').empty();
            $('#city_id').append('<option value="0">Select</option>');
            if (data.length > 0) {
                for (var i = 0; i <= data.length; i++) {
                    $('#city_id').append('<option value="' + data[i]['id'] + '">' + data[i]['city'] + '</option>');
                }
            }
        }
    });
}

function ___calcDiscountAmount(thisObj, serv, subserv, fault, indx) {
    var trId = (serv + subserv + fault).replace(/ /g, '-');
    if ($(thisObj).attr('id') == 'Rate1' + trId) {
        var repeat = parseInt($(thisObj).parent('td').attr('rowspan'));
        var amount = parseFloat($('#Rate1' + trId).val());
        for (var ii = 1; ii <= repeat; ii++) {
            var discount = parseFloat($('#Dis' + ii + trId).val());
            var gtAmount = 0;
            if (!isNaN(amount) && !isNaN(discount)) gtAmount = amount - ((amount * discount) / 100);
            $('.gtAmount' + ii + trId).val(gtAmount);
        }
    } else {
        var amount = parseFloat($('#Rate1' + trId).val());
        var discount = parseFloat($('#Dis' + indx + trId).val());
        var gtAmount = 0;
        if (!isNaN(amount) && !isNaN(discount)) gtAmount = amount - ((amount * discount) / 100);
        $('.gtAmount' + indx + trId).val(gtAmount);
    }
}

function ___calcRateCardAmount(ind) {
    var amount = parseFloat($('#amount' + ind).val());
    var qty = parseFloat($('#qty' + ind).val());
    var discount = parseFloat($('#discount' + ind).val());
    var gtAmount = 0;
    if (!isNaN(amount) && !isNaN(qty)) {
        gtAmount = parseFloat(amount) * parseFloat(qty);
        if (!isNaN(discount)) gtAmount = gtAmount - ((gtAmount * discount) / 100);
    }
    $('#gtAmount' + ind).val(gtAmount);
}

function ___SaveRateCardDataWithServiceAndSubService() {
    var param = $('#RateCardFormData').serialize() + '&_token=' + csrf_token;
    $.ajax({
        type: 'post',
        url: '/api/save_card_rate',
        data: param,
        dataType: 'json',
        success: function (data) {
            var msg = '';
            if (data == 1) msg = 'Successfully Rate Card Inserted & Updated!';
            else if (data == 2) msg = 'Successfully Rate Card Inserted!';
            else if (data == 3) msg = 'Successfully Rate Card Updated!';
            if (msg != '') {
                $('#partname').val('');
                $("#amount").val('');
                $("#units").val('');
                $("#qty").val('');
                $("#discount").val('');
                $("#gtAmount").val('');

                $(".close").trigger('click');
                Toast.fire({
                    type: 'success',
                    title: msg
                });
            }
        }
    });
}

function ___setCityOnSelect() {
    $('#city').val($('#city_id option:selected').text());
}

function ___setSelectOptionTextValueToHiddenField(sltId, hdnId) {
    $('#' + hdnId).val($('#' + sltId + ' option:selected').text());
}

function ___singleRecordDeletion(recId, tabId) {
    var parameters = 'tabId=' + tabId;
    parameters += '&recId=' + recId;
    $.ajax({
        type: 'post',
        url: '/api/deleteaction',
        data: parameters,
        dataType: 'text',
        success: function () {
            $("#DataTable").dataTable().fnDraw();
            location.reload();
        }
    });
}

function ___getExams() {
    var parameters = 'course_id=' + $('#branch_id option:selected').val();
    $.ajax({
        type: 'get',
        url: '/api/getExams',
        data: parameters,
        dataType: 'json',
        success: function (data) {
            //$('#course_id').val($('#course_id option:selected').text());
            // $('#branch_id').empty();
            $('#branch_id').append('<option value="0">Select</option>');
            if (data.length > 0) {
                for (var i = 0; i <= data.length; i++) {
                    $('#branch_id').append('<option value="' + data[i]['id'] + '">' + data[i]['exam_name'] + '</option>');
                }
            }
        }
    });
}

var __FaultDropDownData = '';

function ___drawHtmlData(imgSrc, dKey, dVal, dClass, act, sVal) {
    var __dValClass = dVal.replace(/ /gi, '_').toLowerCase();
    var ___setOnClickFunction = '';

    if (act == 'services') {
        var DivClass = ((__dValClass + act).replace(/ /gi, '_')).toLowerCase();
        ___setOnClickFunction = "___clickToAddSubService('" + dClass + "', '" + dKey + "', '" + dVal + "', '" + DivClass + "')";
    } else if (act == 'subservices') {
        var DivClass = ((__dValClass + sVal + act).replace(/ /gi, '_')).toLowerCase();
        var city_id = $('#city_id option:selected').val();
        var booking_date = $('#booking_date').val();
        ___setOnClickFunction = "__getAdditionalOffers('" + city_id + "', '" + sVal + "', '" + dVal + "', '" + booking_date + "', '" + dKey + "', '" + DivClass + "'); ";
    }
    var __HtmlData = '<div class="ServDiv ' + act + ' ' + DivClass + '" align="center" data-toggle="tooltip" title="Click to Show Sub Services." onclick="' + ___setOnClickFunction + '">';
    __HtmlData += '<div style="width:100%; position: relative;">';
    __HtmlData += '<img src="/upload/serve_images/' + imgSrc + '" style="width:158px;height:120px;border-bottom:1px groove #EFEFEF;"/>';
    __HtmlData += '<p class="' + act + ' ' + DivClass + '" style="text-align:center;width:158px;padding:5px;margin:0px; background-color:#EFEFEF;">' + dVal + '</p>';
    __HtmlData += '</div>';
    __HtmlData += '</div>';
    return __HtmlData;
}
var ___pregroupName = '';

function ___LoadGroupAndServices() {
    var __ik = 1;
    $('.___GroupAndServices').html('');
    $('.___SubServices').html('');
    if (___GroupAndServices.length == 0) {
        alert("Service Not available in " + $('#city_id option:selected').text() + ' city.');
        $('.___GroupAndServices').html('');
        $('.___SubServices').html('');
        if ($('.tableData')) $('.tableData').remove();
        if ($('.rowTotalBar')) $('.rowTotalBar').remove();
        return false;
    } else {
        //$('.___GroupAndServices').removeClass('d-none');
        // /$('.___SubServices').removeClass('d-none');
    }
    $('.___GroupAndServices').append('<div class="row mt-3"><div class="col-md-12"><span>&nbsp;&nbsp;Please Choose Service to Booking Complaints</span></div></div>');
    for (__groupName in ___GroupAndServices) {
        if (___pregroupName != '' && ___pregroupName != __groupName) {
            __ik++;
            ___pregroupName = '';
        }
        for (__k in ___GroupAndServices[__groupName]) {
            var __imgSrc = ___GroupAndServices[__groupName][__k]['img'];
            var __val = ___GroupAndServices[__groupName][__k]['val'];
            var __dClass = __groupName.replace(/ /gi, '_').toLowerCase();
            var ___HtmlData = ___drawHtmlData(__imgSrc, __k, __val, __dClass, 'services', '');
            $('.___GroupAndServices').append(___HtmlData);
        }
        ___pregroupName = __groupName;
    }
}

function ___clickToAddSubService(act, dKey, sVal, DivClass) {
    $('.services').removeClass('ServDivSelected');
    $('.services').removeClass('SelectedText');
    $('.' + DivClass).addClass('ServDivSelected');
    $('.' + DivClass).addClass('SelectedText');
    var __dValClass = sVal.replace(/ /gi, '_').toLowerCase();
    var ___HtmlData = '';
    for (__k in __SubServices[dKey]) {
        var __imgSrc = __SubServices[dKey][__k]['img'];
        var __val = __SubServices[dKey][__k]['val'];
        ___HtmlData += ___drawHtmlData(__imgSrc, __k, __val, __dValClass, 'subservices', sVal);
    }
    $('.___SubServices').html('<div class="__div_' + __dValClass + '"><p><strong>&nbsp;&nbsp;' + sVal + ' &rArr; Sub Services</strong></p>' + ___HtmlData + '</div><div style="clear:both;"></div>');
    $('#serv').val(sVal);
    $('#serv_id').val(dKey);
}

function __getAdditionalOffers(city_id, serv, subserv, booking_date, subServID, DivClass) {
    var parameters = 'city_id=' + city_id;
    parameters += '&serv=' + serv;
    parameters += '&subserv=' + subserv;
    parameters += '&booking_date=' + booking_date;

    $('#DataAODModelContent').html('');
    $('.___AdditionalOfferData').html('');

    $.ajax({
        type: 'get',
        url: '/api/get-additional-offers',
        data: parameters,
        dataType: 'json',
        success: function (data) {
            if (data == 2) __AFFlag = false;
            else {
                __AFFlag = true;
                __AdditionalFaults = data;
                var htmlData = '<p>Offer Valid for ' + __AdditionalFaults[serv][subserv]['offer_for'] + '<p>';
                htmlData += '<table class="table table-sm table-striped table-bordered table-hover">';
                htmlData += '<thead class="thead-dark">';
                htmlData += '<tr style="height:25px;">';
                htmlData += '<th class="w-5 text-center">S.No.</th>';
                htmlData += '<th class="w-49">Service &rArr; SubService &rArr; Fault</th>';
                htmlData += '<th class="w-12 text-center">Qty</th>';
                htmlData += '<th class="w-11 text-center">Discount</th>';
                htmlData += '<th class="w-11 text-center">Amount</th>';
                htmlData += '<th class="w-12 text-center">Firm Points</th>';
                htmlData += '</tr>';
                htmlData += '</thead>';
                for (var service in __AdditionalFaults) {
                    for (var subservice in __AdditionalFaults[service]) {
                        var nos = 1;
                        for (var fault in __AdditionalFaults[service][subservice]) {
                            for (var faultData in __AdditionalFaults[service][subservice][fault]['data']) {
                                htmlData += '<tr>';
                                htmlData += '<td class="text-right">' + nos + '.</td>';
                                htmlData += '<td>' + service + ' &rArr; ' + subservice + ' &rArr; ' + fault + '</td>';
                                htmlData += '<td class="text-center">' + faultData + '</td>';
                                htmlData += '<td class="text-right">' + __AdditionalFaults[service][subservice][fault]['data'][faultData]['discount'] + '%</td>';
                                htmlData += '<td class="text-right">' + __AdditionalFaults[service][subservice][fault]['data'][faultData]['amount'] + '</td>';
                                htmlData += '<td class="text-right">' + __AdditionalFaults[service][subservice][fault]['data'][faultData]['points'] + '</td>';
                                htmlData += '</tr>';
                                nos++;
                            }
                        }
                    }
                }
                htmlData += '</table>';
                $('#DataAODModelContent').html(htmlData);
                $('.___AdditionalOfferData').html('<button type="button" data-toggle="modal" data-target="#myOfferModal" class="btn btn-success btn-xs pr-2 pl-2">View Applied Offer</button>');
            }
            ___clickToAddDataToCreateComplaint(subServID, subserv, serv, DivClass);
        }
    });
}

function ___clickToAddDataToCreateComplaint(subServID, subServ, mainServ, DivClass) {
    var __trId = subServ.replace(/ /gi, '_').toLowerCase();
    $('.rowTotalBar').remove();
    $('#ErrorTr').hide();
    $('.tableData').remove();
    $('.subservices').removeClass('ServDivSelected');
    $('.subservices').removeClass('SelectedText');
    $('.' + DivClass).addClass('ServDivSelected');
    $('.' + DivClass).addClass('SelectedText');
    $('#subserv').val(subServ);
    $('#subserv_id').val(subServID);
    var __ik = 1;
    for (__kServ in __Faults) {
        if (__kServ == mainServ) {
            for (__kSubServ in __Faults[__kServ]) {
                if (__kSubServ == subServ) {
                    for (__k in __Faults[__kServ][__kSubServ]) {
                        var FaultName = __Faults[__kServ][__kSubServ][__k]['fn'];
                        if (__AFFlag == true) {
                            var FaultRate = __AdditionalFaults[__kServ][__kSubServ][FaultName]['faultrate'];
                        } else var FaultRate = __Faults[__kServ][__kSubServ][__k]['rt'];
                        var ___htmlDataTable = '<tr class="tableData" style="height:25px;" id="__tr_' + __trId + '">';
                        ___htmlDataTable += '<td align="right"><strong>' + __ik + '.</strong></td>';
                        ___htmlDataTable += '<td align="left">&nbsp;' + FaultName + '</td>';
                        ___htmlDataTable += '<td align="center">';
                        ___htmlDataTable += '<input type="button" value="-" onclick="qtyMinus(this);" class="qtyminus" field="inp_qty_' + __trId + __ik + '" />';
                        ___htmlDataTable += '<input name="fault[' + FaultName + '][qty]" type="text" maxlength="2" id="inp_qty_' + __trId + __ik + '" value="" onkeyup="___getDiscountAmoutAsPerQty(this, \'' + __kServ + '\',\'' + __kSubServ + '\', \'' + FaultName + '\', \'' + __trId + '\', \'' + __ik + '\'); ___setAmountAsPerRateUnitAndQty(this, \'' + __ik + '\');" class="text-right w-40 inp_qty"/>';
                        ___htmlDataTable += '<input type="button" value="+" onclick="qtyPlus(this);"  class="qtyplus" field="inp_qty_' + __trId + __ik + '" /></td>';
                        ___htmlDataTable += '<td align="center"><input name="fault[' + FaultName + '][unit]" type="text" id="inp_rpu_' + __trId + __ik + '" value="' + FaultRate + '" onkeyup="___setAmountAsPerRateUnitAndQty(this, \'' + __ik + '\');" class="text-right w-70"/></td>';
                        ___htmlDataTable += '<td align="right"><input name="fault[' + FaultName + '][discount]" onkeyup="___setAmountAsPerRateUnitAndQty(this, \'' + __ik + '\');"  type="text" class="text-right w-70" id="st_dis_' + __trId + __ik + '" value="" /><span id="sym_dis_' + __trId + __ik + '">%</span>&nbsp;</td>';
                        ___htmlDataTable += '<td align="right"><strong class="st_gamt" id="st_gamt_' + __trId + __ik + '">--</strong>&nbsp;</td>';
                        ___htmlDataTable += '<td align="right"><strong class="st_dis_amt" id="st_dis_amt_' + __trId + __ik + '">--</strong>&nbsp;</td>';
                        ___htmlDataTable += '<td align="right"><strong class="st_amt" id="st_amt_' + __trId + __ik + '">--</strong>&nbsp;</td>';
                        ___htmlDataTable += '</tr>';
                        $('#DataTable').append(___htmlDataTable);
                        __ik++;
                    }
                }
            }
            var ___htmlDataTable = '<thead class="thead-dark rowTotalBar"><tr style="height:25px;">';
            ___htmlDataTable += '<th class="text-right" colspan="2"><strong>Grand Total</strong>&nbsp;</th>';
            ___htmlDataTable += '<th class="text-right"><strong id="st_qty">--</strong>&nbsp;</th>';
            ___htmlDataTable += '<th colspan="2"></th>';
            ___htmlDataTable += '<th class="text-right"><strong id="st_qtyrate">--</strong>&nbsp;</th>';
            ___htmlDataTable += '<th class="text-right"><strong id="st_disamt">--</strong>&nbsp;</th>';
            ___htmlDataTable += '<th class="text-right"><strong id="st_gtotalamt">--</strong>&nbsp;</th>';
            ___htmlDataTable += '</tr></thead>';
            $('#DataTable').append(___htmlDataTable);
        }
    }
}



function qtyPlus(thisid)
{
    fieldName = $(thisid).attr('field');
    var currentVal = parseInt($('#' + fieldName).val()); // Get its current value
    if (!isNaN(currentVal)) {
        $('#' + fieldName).val(currentVal + 1); // Increment
    } else {
        $('#' + fieldName).val(1); // Otherwise put a 0 there
    }
    $('#' + fieldName).keyup();
}

function qtyMinus(thisid) {
    fieldName = $(thisid).attr('field');
    var currentVal = parseInt($('#' + fieldName).val()); // Get its current value
    if (!isNaN(currentVal) && currentVal > 0) {
        $('#' + fieldName).val(currentVal - 1); // Decrement one
    } else {
        $('#' + fieldName).val(0); // Otherwise put a 0 there
    }
    $('#' + fieldName).keyup();

}

function ___getDiscountAmoutAsPerQty(obj, kServ, kSubServ, FaultName, trId, __ik) {
    var currentDiscount = $('#st_dis_' + trId + __ik).val()
    if (!isNaN(currentDiscount) && currentDiscount > 0) {
        $('#st_dis_' + trId + __ik).val(currentDiscount);
    } else {
        if (__AFFlag == true) {
            for (qty in __AdditionalFaults[kServ][kSubServ][FaultName]['data']) {
                var QtyArr = qty.split('-');
                var objVal = $(obj).val();
                if (objVal >= QtyArr[0] && objVal <= QtyArr[1]) {
                    var discountVal = __AdditionalFaults[kServ][kSubServ][FaultName]['data'][qty]['discount'];
                    $('#st_dis_' + trId + __ik).val(discountVal);
                    break;
                } else {
                    $('#st_dis_' + trId + __ik).val('');
                    $('#st_dis_amt_' + trId + __ik).html('');
                }
            }
        } else {
            for (qty in __Discounts[kServ][kSubServ][FaultName]) {
                var QtyArr = qty.split('-');
                var objVal = $(obj).val();
                if (objVal >= QtyArr[0] && objVal <= QtyArr[1]) {
                    var discountVal = __Discounts[kServ][kSubServ][FaultName][qty];
                    $('#st_dis_' + trId + __ik).val(discountVal);
                    break;
                } else {
                    $('#st_dis_' + trId + __ik).val('');
                    $('#st_dis_amt_' + trId + __ik).html('');
                }
            }
        }
    }

}

function ___setFinalTotalRowData() {
    var __ttlQty = 0;
    var __ttlGAmt = 0;
    var __ttlDAmt = 0;
    var __ttlTAmt = 0;
    $('.inp_qty').each(function () {
        var tmp = parseInt($(this).val().replace(/,/gi, ''));
        if (!isNaN(tmp)) __ttlQty = __ttlQty + tmp;
    });
    $('.st_gamt').each(function () {
        var tmp = parseInt($(this).html().replace(/,/gi, ''));
        if (!isNaN(tmp)) __ttlGAmt = __ttlGAmt + tmp;
    });
    $('.st_dis_amt').each(function () {
        var tmp = parseInt($(this).html().replace(/,/gi, ''));
        if (!isNaN(tmp)) __ttlDAmt = __ttlDAmt + tmp;
    });
    $('.st_amt').each(function () {
        var tmp = parseInt($(this).html().replace(/,/gi, ''));
        if (!isNaN(tmp)) __ttlTAmt = __ttlTAmt + tmp;
    });
    $('#st_qty').html(eval(stripNonNumeric(__ttlQty)).formatMoney(0, '.', ','));
    $('#st_qtyrate').html(eval(stripNonNumeric(__ttlGAmt)).formatMoney(0, '.', ','));
    $('#st_disamt').html(eval(stripNonNumeric(__ttlDAmt)).formatMoney(0, '.', ','));
    $('#st_gtotalamt').html(eval(stripNonNumeric(__ttlTAmt)).formatMoney(0, '.', ','));
}

function ___setAmountAsPerRateUnitAndQty(inpObj, __ik) {
    var __trID = $(inpObj).parent().parent().attr('id').replace('__tr_', '');
    var __qty = parseInt($('#inp_qty_' + __trID + __ik).val());
    var __ratePerUnit = parseInt($('#inp_rpu_' + __trID + __ik).val());
    var __discountPer = parseInt($('#st_dis_' + __trID + __ik).val());

    var __amount = Math.round(__qty * __ratePerUnit);
    if (!isNaN(__amount)) {
        $('#st_gamt_' + __trID + __ik).html(eval(stripNonNumeric(__amount)).formatMoney(0, '.', ','));
        if (!isNaN(__discountPer)) {
            var disCountedAmt = parseInt((__amount * __discountPer) / 100);
            __amount = __amount - disCountedAmt;
            $('#st_dis_amt_' + __trID + __ik).html(eval(stripNonNumeric(disCountedAmt)).formatMoney(0, '.', ','));
        }
        $('#st_amt_' + __trID + __ik).html(eval(stripNonNumeric(__amount)).formatMoney(0, '.', ','));
    } else {
        $('#st_gamt_' + __trID + __ik).html('--');
        $('#st_amt_' + __trID + __ik).html('--');
    }
    ___setFinalTotalRowData();
}

function ___setFaultRatePerUnit(__sltObj) {
    var __trID = $(__sltObj).parent().parent().attr('id').replace('__tr_', '');
    var ___rateperUnit = __Faults[__sltObj.value]['rt'];
    $('#inp_rpu_' + __trID).val(___rateperUnit);
}

function ___LoadFaults() {
    var ___htmlData = '<select style="width:80%" onChange="___setFaultRatePerUnit(this);"><option value="">Select</option>';
    for (__k in __Faults) {
        ___htmlData += '<option data-index="' + __Faults[__k]['rt'] + '" value="' + __k + '">';
        ___htmlData += __Faults[__k]['fn'];
        ___htmlData += '</option>';
    }
    ___htmlData += '</select>';
    return ___htmlData;
}

function stripNonNumeric(str) {
    str += '';
    var rgx = /^\d|\.|-$/;
    var out = '';
    for (var i = 0; i < str.length; i++) {
        if (rgx.test(str.charAt(i))) {
            if (!((str.charAt(i) == '.' && out.indexOf('.') != -1) || (str.charAt(i) == '-' && out.length != 0)) || str.match(/\D/g)) {
                out += str.charAt(i);
            }
        }
    }
    return out;
}

function format_price(theControl) {
    var num = stripNonNumeric(theControl.value);
    num = eval(num);
    if (!isNaN(num)) theControl.value = num.formatMoney(0, '.', ',');
    else theControl.value = '';
}
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "," : d,
        t = t == undefined ? "." : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function ___getLoadEstimatedAmountDetails(Qty, unitRate, UnitAmount, Discount, DiscountedAmount, gAmount) {
    var htmlData = '';
    htmlData += '<div class="row">';
    htmlData += '<div class="col-md-4 text-center"><strong>Quantity: </strong><strong>' + eval(stripNonNumeric(Qty)).formatMoney(0, '.', ',') + '</strong></div>';
    htmlData += '<div class="col-md-4 text-center"><strong>Unit Rate: </strong><strong>' + eval(stripNonNumeric(unitRate)).formatMoney(0, '.', ',') + '</strong></div>';
    htmlData += '<div class="col-md-4 text-center"><strong>Discount: </strong><strong>' + eval(stripNonNumeric(Discount)).formatMoney(0, '.', ',') + '%</strong></div>';
    htmlData += '</div>';
    htmlData += '<hr/>';
    htmlData += '<div class="row mt-2">';
    htmlData += '<div class="col-md-10 text-right"><strong>Unit Amount</strong></div>';
    htmlData += '<div class="col-md-2 text-right"><strong>' + eval(stripNonNumeric(UnitAmount)).formatMoney(0, '.', ',') + '<i class="fa fa-rupee-sign ml-1"></i></strong></div>';
    htmlData += '</div>';
    htmlData += '<div class="row">';
    htmlData += '<div class="col-md-10 text-right"><strong>Discounter Amount</strong></div>';
    htmlData += '<div class="col-md-2 text-right"><strong>' + eval(stripNonNumeric(DiscountedAmount)).formatMoney(0, '.', ',') + '<i class="fa fa-rupee-sign ml-1"></i></strong></div>';
    htmlData += '</div>';
    htmlData += '<hr/>';
    htmlData += '<div class="row">';
    htmlData += '<div class="col-md-10 text-right"><strong>Total Amount</strong></div>';
    htmlData += '<div class="col-md-2 text-right"><strong>' + eval(stripNonNumeric(gAmount)).formatMoney(0, '.', ',') + '<i class="fa fa-rupee-sign ml-1"></i></strong></div>';
    htmlData += '</div>';
    $('#DataModelContent').html(htmlData);
}

$('input[type="file"]').change(function () {
    var extArr = this.value.split('.');
    var ext = extArr[parseInt(extArr.length) - 1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'bmp':
        case 'pdf':
            /*case 'doc':
            case 'docx':*/
            $(this).removeAttr('data-toggle', 'tooltip');
            $(this).siblings(".custom-file-label").removeClass('alert-danger');
            $(this).siblings(".custom-file-label").addClass('alert-success');
            break;
        default:
            $('input[type="file"]').val('');
            $(this).attr('data-toggle', 'tooltip');
            $(this).siblings(".custom-file-label").removeClass('alert-success');
            $(this).siblings(".custom-file-label").addClass('alert-danger');
    }
});

function readURL(input, imgId) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#' + imgId).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function __loadOfferData() {
    var cityID = $('#city_id').val();
    var serviceID = $('#service_id').val();
    var sub_service_id = $('#sub_service_id').val();
    var ErrorText = '';
    //if($('#offer_date').val()=='')      ErrorText+='Offer Date\n';
    //if($('#offer_end_date').val()=='')  ErrorText+='Offer EndDate\n';
    if (cityID == '') ErrorText += 'Select City\n';
    if (serviceID == '') ErrorText += 'Select Service\n';
    if (sub_service_id == '') ErrorText += 'Select Sub-Service\n';
    if (ErrorText != '') {
        alert(ErrorText);
        return false;
    } else {

        $.ajax({
            type: 'post',
            url: '/api/load-offer-data',
            data: $('#frm_search_params').serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.length > 0) {
                    var htmlRateData = '';
                    var htmlHourData = '';

                    for (k in data) {
                        console.log(data);
                        htmlRateData += ___drawFaultRowHtmlData(data, parseInt(k), 'rate');
                        htmlHourData += ___drawFaultRowHtmlData(data, k, 'hour');
                    }
                    $('#tableRateData').html(htmlRateData);
                    $('#tableHourData').html(htmlHourData);
                    ___fileQtyComboBoxData();
                    $('.offer_qty_but').removeClass('d-none');
                    $('.offer_qty_hour_but').removeClass('d-none');
                }
            }
        });
    }
    return false

}
