@extends('admin/layouts/default')
@section('title')
<title>Tata Green - Dashboard</title>
@stop
@section('inlinecss')
 <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop
@section('breadcrum')
<h1 class="page-title">Dashboard</h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">dashboard</li>
</ol>
@stop

@section('content')
<div class="app-content">
    <div class="side-app">
        @include('admin.layouts.pagehead')
        
         <!-- Users Section -->
        <div class="row">
            <div class=" col-md-12 col-lg-12 col-xl-12">
                <div class="row">
                    	<div class="col-sm-12 col-lg-2 col-md-2 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$Totalleads}}</h2>
                                    <a href="{{route('leads-list')}}" >Total Leads</a>
                                </div>
                            </div>						
                        </div>
                        
                        	<div class="col-sm-12 col-lg-2 col-md-2 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$Closedleads}}</h2>
                                    <p>Closed Leads</p>
                                </div>
                            </div>						
                        </div>
                        
                        	<div class="col-sm-12 col-lg-2 col-md-2 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$Cancelledleads}}</h2>
                                    <p>Cancelled Leads</p>
                                </div>
                            </div>						
                        </div>
                    
                        <div class="col-sm-12 col-lg-2 col-md-2 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$Openleads}}</h2>
                                    <p>Open Leads</p>
                                </div>
                            </div>						
                        </div>
                        
    				
    
    				

	                    <div class="col-sm-12 col-lg-2 col-md-2 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$Resheduedleads}}</h2>
                                    <p>Re-Scheduled Leads</p>
                                </div>
                            </div>						
                        </div>

	                    <div class="col-sm-12 col-lg-2 col-md-2 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$Contact1}}</h2>
                                    <p>1st Contact Esablished Leads</p>
                                </div>
                            </div>						
                        </div>
                        
                </div>
            </div>
             @if(auth()->user()->hasRole('Super Admin'))
             <div class=" col-md-12 col-lg-12 col-xl-12">
                <div class="row">
                    	<div class="col-sm-12 col-lg-2 col-md-3 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$Openleads+$Resheduedleads+$Contact1}}</h2>
                                    Open / Re-Scheduled /1st Contact Leads
                                </div>
                            </div>						
                        </div>
                        <div class="col-sm-12 col-lg-2 col-md-3 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$leadwithzero}}</h2>
                                    Escalation with Zero
                                </div>
                            </div>						
                        </div>
                        <div class="col-sm-12 col-lg-2 col-md-3 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$leadwithone}}</h2>
                                    Escalation with One
                                </div>
                            </div>						
                        </div>
                        <div class="col-sm-12 col-lg-2 col-md-3 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$leadwithtwo}}</h2>
                                    Escalation with Two
                                </div>
                            </div>						
                        </div>
                    
                        <div class="col-sm-12 col-lg-2 col-md-3 ">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="mb-1 number-font">{{$leadwithmoretwo}}</h2>
                                    Escalation with More Than Two
                                </div>
                            </div>						
                        </div>
    
    				
                        
                </div>
            </div>
            @endif
            
               <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
                    <div class="card  overflow-hidden">
                        <div class="card-header">
                            <h3 class="card-title">Leads</h3>
                        </div>
                        <div class="card-body text-center">
                            <div id="morrisBar8" class="h-340 donutShadow"></div>
                            
                            <div class="mt-2 text-center">
 
                                <span class="dot-label " style="background-color:#ffbb33"></span><span class="mr-3">Open Leads</span>
                                <span class="dot-label " style="background-color:#33b5e5"></span><span class="mr-3">Re-Scheduled Leads</span>
                                <span class="dot-label "style="background-color:#00C851"></span><span class="mr-3">Closed Leads</span>
                                <span class="dot-label "style="background-color:#ff4444"></span><span class="mr-3">Cancelled Leads</span>
                                <span class="dot-label "style="background-color:#000000"></span><span class="mr-3">1st Contact Esablished Leads</span>
                                
                            </div>
                        </div>
                    </div>
                </div><!-- COL END -->

               <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
                    <div class="card  overflow-hidden">
                        <div class="card-header">
                            <h3 class="card-title">Lead From</h3>
                        </div>
                        <div class="card-body text-center">
                            <div id="morrisBar9" class="h-340 donutShadow"></div>
                            
                            <div class="mt-2 text-center">
                                <span class="dot-label " style="background-color:#ff4444"></span><span class="mr-3">Parivaar</span>
                                <span class="dot-label " style="background-color:#ffbb33"></span><span class="mr-3">PreBook</span>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                
                
                 @if(auth()->user()->hasRole('Super Admin'))
               <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card  overflow-hidden">
                        <div class="card-header">
                            <h3 class="card-title">Lead State Wise</h3>
                        </div>
                        <div class="card-body text-center">
                            <table class="table table-bordered data-table">
                                <tr>
                                    <td>S.no</td>
                                    <td>State</td>
                                    <td>Total Leads</td>
                                    <td style="background-color:#ffbb33;color:white">Open Leads</td>
                                    <td style="background-color:#ff4444;color:white">Cancelled Leads</td>
                                    <td style="background-color:#00C851;color:white">Closed Leads</td>
                                    <td style="background-color:#33b5e5;color:white">Re-Scheduled Leads</td>
                                    <td style="background-color:#000000;color:white">1st Contact Esablished Leads</td>
                                </tr>
                                
                                @foreach($states as $keys=>$vals)
                                
                                <tr>
                                    <td>{{++$keys}}</td>
                                    <td>{{$vals->name}}</td>
                                    <td>{{$vals->total_leads}}</td>
                                    <td style="background-color:#ffbb33;color:white">{{$vals->open_leads}}</td>
                                    <td style="background-color:#ff4444;color:white">{{$vals->cancelled_leads}}</td>
                                    <td style="background-color:#00C851;color:white">{{$vals->closed_leads}}</td>
                                    <td style="background-color:#33b5e5;color:white">{{$vals->rescheduled_leads}}</td>
                                    <td style="background-color:#000000;color:white">{{$vals->first_leads}}</td>
                                </tr>

                                
                                @endforeach
                                
                            </table>
                            
                        </div>
                    </div>
                </div>
                 @endif
                
        </div>
        
    </div>
</div>
@stop

@section('inlinejs')



<!-- SPARKLINE JS-->
<script src="{{ asset('admin/assets/js/jquery.sparkline.min.js') }}"></script>
<!-- CHART-CIRCLE JS -->
<script src="{{ asset('admin/assets/js/circle-progress.min.js') }}"></script>
<!-- RATING STAR JS-->
<script src="{{ asset('admin/assets/plugins/rating/jquery.rating-stars.js') }}"></script>
<!-- CHARTJS CHART JS-->
<script src="{{ asset('admin/assets/plugins/chart/Chart.bundle.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/chart/utils.js') }}"></script>
<!-- C3.JS') }} CHART JS -->
<script src="{{ asset('admin/assets/plugins/charts-c3/d3.v5.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/charts-c3/c3-chart.js') }}"></script>
<!-- INPUT MASK JS-->
<script src="{{ asset('admin/assets/plugins/input-mask/jquery.mask.min.js') }}"></script>
<!-- CHARTJS CHART JS -->
<script src="{{ asset('admin/assets/plugins/chart/Chart.bundle.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/chart/utils.js') }}"></script>
<!-- PIETY CHART JS-->
<script src="{{ asset('admin/assets/plugins/peitychart/jquery.peity.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/peitychart/peitychart.init.js') }}"></script>
<!--MORRIS js-->
<script src="{{ asset('admin/assets/plugins/morris/morris.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/morris/raphael-min.js') }}"></script>
<!-- ECharts JS -->
<script src="{{ asset('admin/assets/plugins/echarts/echarts.js') }}"></script>
<!-- INDEX JS-->
<script src="{{ asset('admin/assets/js/index4.js') }}"></script>
@php

$total = $Totalleads - $Openleads - $Closedleads - $Cancelledleads - $Resheduedleads;
@endphp
@if($Totalleads>0)
<script>
    /*---- MorrisDonutChart----*/
	new Morris.Donut({
		element: 'morrisBar8',
		data: [
		    {
			value: {{number_format(($Openleads/$Totalleads)*100)}},
			label: 'Open Leads'
		    },
            {
			value: {{number_format(($Resheduedleads/$Totalleads)*100)}},
			label:'Re-Scheduled Leads'
		    },
		    {
			value: {{number_format(($Closedleads/$Totalleads)*100)}},
			label: 'Closed Leads'
		    },
		    {
		     value:{{number_format(($Cancelledleads/$Totalleads)*100)}},
		     label: 'Cancelled Leads'
		    },
		    {
		     value:{{number_format(($Contact1/$Totalleads)*100)}},
		     label: '1st Contact Esablished Leads'
		    }

		    
		    ],
		backgroundColor: 'rgba(119, 119, 142, 0.2)',
		labelColor: '#77778e',
		colors: ['#ffbb33','#33b5e5','#00C851','#ff4444','#000000'],
		formatter: function(x) {
			return x + "%"
		}
	}).on('click', function(i, row) {
		console.log(i, row);
	});


    /*---- MorrisDonutChart----*/
	new Morris.Donut({
		element: 'morrisBar9',
		data: [
		    {
			value: {{number_format(($leadsFromFamily/$Totalleads)*100,2)}},
			label: 'Parivaar'
		    },
		    {
			value: {{number_format(($leadsFromPrebook/$Totalleads)*100,2)}},
			label: 'PreBook'
		    }
		    ],
		backgroundColor: 'rgba(119, 119, 142, 0.2)',
		labelColor: '#77778e',
		colors: ['#ff4444', '#ffbb33'],
		formatter: function(x) {
			return x + "%"
		}
	}).on('click', function(i, row) {
		console.log(i, row);
	});

</script>


@endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
       $('.data-table').DataTable();
</script>

@stop