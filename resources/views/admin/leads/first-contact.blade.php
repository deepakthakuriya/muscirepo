@extends('admin/layouts/default')
@section('title')
<title>Leads List</title>
@stop
@section('inlinecss')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@stop
@section('content')
<div class="app-content">
    <div class="side-app">

        <!-- PAGE-HEADER -->
        @include('admin.layouts.pagehead')
        <!-- PAGE-HEADER END -->

        <!-- ROW-1 OPEN -->
        <div class="col-12">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Leads List</h3>
                        <div class="ml-auto pageheader-btn">
                         {{--   <form action="{{route('export-data')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                       <div class="form-group">
                                           <label>to</label>
                                           <input class="form-control" type="text" name="todate" autocomplete="off" id="todate" />
                                       </div>
                                    </div>
                                     <div class="col-md-4">
                                       <div class="form-group">
                                           <label>from</label>
                                           <input class="form-control" type="text" name="fromdate" autocomplete="off" id="fromdate" />
                                       </div>
                                     </div>
                                    <div class="col-md-4">
                                     <button class="btn btn-primary">Export</button>
                                    </div>
                                </div>
                              
                            </form>--}}
						</div>
                    </div>
                    <div class="card-body ">
                         <table class="table table-bordered data-table">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>E-mail</th>
                              <!--<th>Mobile</th>-->
                              <th>States</th>
                              <th>Created At</th>
                              <th>Closed Time</th>
                              <th>Turn Around</th>
                              <th>escalation</th>
                               <th>Lead Status</th>
                              <th width="100px">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>

                    

                    </div>
                </div>
            </div>
        </div>
        <!-- ROW-1 CLOSED -->
    </div>

    <!-- View MODAL -->
<div class="modal fade" id="viewDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

			</div>
		</div>
	</div>
</div>
<!-- View CLOSED -->





</div>
@stop
@section('inlinejs')
 

   	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
    
    <script type="text/javascript">
         $(function () {
            
            var todate = $('#todate').val();
            var fromdate = $('#fromdate').val();
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
                ajax: "{{ route('first-Contact-list') }}"+'?todate='+todate+'&fromdate='+fromdate,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    // {data: 'mobile', name: 'mobile'},
                    {data: 'state', name: 'state'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'closed_at', name: 'closed_at'},
                    {data: 'turn_around', name: 'turn_around'},
                    {data: 'esclation_status', name: 'esclation_status'},
                    {data: 'lead_status', name: 'lead_status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

         
        });
    
    
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
    </script>
    

@stop
