@extends('admin/layouts/default')
@section('title')
<title>Leads Details </title>
@stop
@section('inlinecss')
<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/tdemes/ui-lightness/jquery-ui.css" />
@stop
@section('breadcrum')
<h1 class="page-title">Leads Details </h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Leads </a></li>
    <li class="breadcrumb-item active" aria-current="page">Details</li>
</ol>
@stop
@section('content')
<style>
#ui-datepicker-div
{
z-index: 999999!important;
}
</style>
<div class="app-content">
    <div class="side-app">
        @include('admin.layouts.pagehead')
        @include('admin.alert')
        <br />
        <div class="card p-3 pt-3" style="overflow: scroll">
             <h4><b>Leads  Detail </h4>
            
           
                <table class="table table-bordered data-table">
                <thead>
                    <tr class="d-none">
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <br />
                <tbody>
                    <tr>
                       <td>User Name</td>
                       <td > {{$leads->name}}</td>
                     </tr>

                    <tr>
                       <td> E-mail</td>
                       <td > {{$leads->email}}</td>
                     </tr>
                     
                    <tr>
                       <td> Mobile No</td>
                       <td >{{$leads->mobile}}</td>
                     </tr>
                     
                  
                    <tr>
                       <td>States</td>
                       <td >{{$leads->state}}</td>
                     </tr>

                    <tr>
                       <td>City</td>
                        <td >{{$leads->city}}</td>
                     </tr>

                    <tr>
                       <td>Category</td>
                       <td > {{$leads->category}}</td>
                     </tr>

                    <tr>
                       <td>Company</td>
                       <td > {{$leads->company}}</td>
                    </tr>

                    <tr>
                       <td>Model</td>
                       <td > {{$leads->model}}</td>
                    </tr>

                    <tr>
                       <td>Battery Type</td>
                       <td > {{$leads->battery_type}}</td>
                    </tr>

                    <tr>
                       <td>Warranty</td>
                       <td > {{$leads->warranty}}</td>
                    </tr>
                    
                    <tr>
                       <td>MRP</td>
                       <td > {{$leads->mrp}}</td>
                    </tr>

                    <tr>
                       <td>Capacity</td>
                       <td > {{$leads->capacity}}</td>
                    </tr>

                    <tr>
                       <td>Group Company</td>
                       <td > {{$leads->group_company}}</td>
                    </tr>

                    <tr>
                       <td>How Do You Know</td>
                       <td > {{$leads->how_do_you_know}}</td>
                    </tr>
    
                    <tr>
                       <td>Lead From</td>
                       <td > {{$leads->Lead_from}}</td>
                    </tr>

                     <tr>
                       <td>Lead Status</td>
                       <td > {{$leads->lead_status}}</td>
                    </tr>

                     <tr>
                       <td>1st contact at</td>
                       <td > {{(isset($contact1->status_cat_name))?$contact1->status_cat_name:''}}</td>
                    </tr>
                    
                     <tr>
                       <td>Created At</td>
                       <td > {{$leads->created_at}}</td>
                    </tr>

                     <tr>
                       <td>Updated By</td>
                       <td > {{$leads->updated_by}}</td>
                    </tr>
                    
                    <tr>
                       <td>Lead Status</td>
                       <td >
                           <button type="button" class="btn btn-primary statusDetail btn-sm" data-toggle="modal" data-target="#statusDetail">Update Status</button>
                       </td>
                    </tr>
                    
                </tbody>
              </table>
              
              @if(auth()->user()->hasRole('Super Admin'))
               <div class="form-group">
                   <button class="btn btn-primary" type="button"  data-toggle="modal" data-target="#locationDetails"> Change Location </button>
               </div>
             @endif            
        </div>
          <div class="col-12">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Status Update List</h3>
                    </div>
                    <div class="card-body ">

                    <table class="table table-bordered data-table">
                      <tdead>
                          <tr>
                              <td>ID</td>
                              <td>Status</td>
                             
                              <td>Comment</td>
                              <td>Reminder Date </td>
                              <td>Created At</td>
                              <td>Updated At</td>
                              <!--<td widtd="100px">Action</td>-->
                          </tr>
                      </tdead>
                      <tbody>
                        @foreach($statusupdate as $key=>$val)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$val->status_cat_name}}</td>
                            <td>{{$val->comment}}</td>
                            <td>{{$val->reminder_date}}</td>
                            <td>{{$val->created_at}}</td>
                            <td>{{$val->updated_at}}</td>
                           
                        </tr>
                        @endforeach
                      </tbody>
                  </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="statusDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Status Update</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form  id="submitForm" action="{{route('status.update',$leads->id)}}">
			    @csrf
    			<div class="modal-body">
    			    <div class="form-group">
    			        <label>Status</label>
    			        <select class="form-control" name="status_cat_id" id="status_cat_id" onchange="getSubStatus()">
    			            <option value="">Select</option>
    			            @foreach($category as $key=>$val)
        			            <option value="{{$val->id}}" @if($val->title==$leads->lead_status) selected @endif data-status-name="{{$val->title}}">{{$val->title}}</option>
                            @endforeach
    			        </select>
    			    </div>


                    <div class="form-group">
    			        <label>Comment</label>
                        <textarea name="comment" class="form-control" id="comment"></textarea>
    			    </div>
                    
                    
                    <div class="form-group @if($leads->lead_status!='Resheduled') d-none @endif" id="reminder-div">
    			        <label>Reminder Date</label>
    			        <input type="text" class="form-control" name="reminder_date" id="reminder_date" />
    			    </div>
    			    
                </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
				 <button type="submit" id="submitButton" class=" text-white btn btn-primary float-right"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Sending..." data-rest-text="Submit">Submit</button>
				
			</div>
    			<input type="hidden" name="status_cat_name" id="status_cat_name"  value="{{$leads->lead_status}}"/>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="locationDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Status Update</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form  id="submitForm2" action="{{route('lead-details-update',$leads->id)}}">
			    @csrf
    			<div class="modal-body">
    			   <div class="form-group">
						<label class="form-label">States *</label>
						<select name="states_id" id="states_id"   class="form-control">
							<option value="">Select</option>
                            @foreach($states as $key=>$val)
								<option  {{ ($leads->state==$val->name)?'selected':''}} value="{{$val->name}}">{{$val->name}}</option>
                            @endforeach
						</select>
					</div>


                   <div class="form-group">
						<label class="form-label">Cities *</label>
					    <input type="text" class="form-control" name="city" value="{{$leads->city}}" id="city" />
					</div>

                </div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
   			    <button type="submit" id="submitButton2" class=" text-white btn btn-primary float-right"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Sending..." data-rest-text="Submit">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>

@stop
@section('inlinejs')
<script src="{{ asset('admin/assets/multiselectbox/js/jquery.multi-select.js') }}"></script>
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    
    $('#states_id').multiSelect();
    
        $(function () {
           
        
            $(".data-table").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ],
                pageLength: 50,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": false,
                "bInfo": false,
                "bAutoWidth": false,
                "bSortable": false,
                "ordering": false
            } );
         

        });
    </script>
  <script>

  $( function() {
    $( "#reminder_date" ).datepicker({
    //comment tde beforeShow handler if you want to see tde ugly overlay
    beforeShow: function() {
        setTimeout(function(){
            $('.ui-datepicker').css('z-index', 99999999999999);
        }, 0);
    }
});
  } );
  </script>
  

<script>


$('#pdfprint').click(function () {
window.print()
});
    
    function getSubStatus(){
        
        var  status_cat_id = $("#status_cat_id option:selected").val();
        var status_cat_name =      $("#status_cat_id option:selected").data('status-name');
            $("#status_cat_name").val($("#status_cat_id option:selected").data('status-name'));
        
            
             if(status_cat_name=='Resheduled')
             {
                 $("#reminder-div").removeClass('d-none');
             }
             else
             {
                 $("#reminder-div").addClass('d-none');                 
             }

    }

     $(function () {

$('#submitForm').submit(function(){
     var $tdis = $('#submitButton');
     buttonLoading('loading', $tdis);
     $('.is-invalid').removeClass('is-invalid state-invalid');
     $('.invalid-feedback').remove();
     $.ajax({
         url: $('#submitForm').attr('action'),
         type: "POST",
         processData: false,  // Important!
         contentType: false,
         cache: false,
         data: new FormData($('#submitForm')[0]),
         success: function(data) {
             if(data.status){
                 var btn = '<a href="#" class="btn btn-info btn-sm">GoTo List</a>';
                 successMsg('Status Update', data.msg, btn);
                   location.reload();
                 $('#submitForm')[0].reset();
              
             }else{
    
                 $.each(data.errors, function(fieldName, field){
                     $.each(field, function(index, msg){
                         $('#'+fieldName).addClass('is-invalid state-invalid');
                        errorDiv = $('#'+fieldName).parent('div');
                        errorDiv.append('<div class="invalid-feedback">'+msg+'</div>');
                     });
                 });
                 errorMsg('Status Update', 'Input Error');
             }
             buttonLoading('reset', $tdis);
    
         },
         error: function() {
             errorMsg('Status Update', 'tdere has been an error, please alert us immediately');
             buttonLoading('reset', $tdis);
         }
    
     });

    return false;
});



$('#submitForm2').submit(function(){
     var $tdis = $('#submitButton2');
     buttonLoading('loading', $tdis);
     $('.is-invalid').removeClass('is-invalid state-invalid');
     $('.invalid-feedback').remove();
     $.ajax({
         url: $('#submitForm2').attr('action'),
         type: "POST",
         processData: false,  // Important!
         contentType: false,
         cache: false,
         data: new FormData($('#submitForm2')[0]),
         success: function(data) {
             if(data.status){
                 var btn = '<a href="#" class="btn btn-info btn-sm">GoTo List</a>';
                 successMsg('Change Location', data.msg, btn);
                   location.reload();
                 //$('#submitForm2')[0].reset();
              
             }else{
    
                 $.each(data.errors, function(fieldName, field){
                     $.each(field, function(index, msg){
                         $('#'+fieldName).addClass('is-invalid state-invalid');
                        errorDiv = $('#'+fieldName).parent('div');
                        errorDiv.append('<div class="invalid-feedback">'+msg+'</div>');
                     });
                 });
                 errorMsg('Change Location', 'Input Error');
             }
             buttonLoading('reset', $tdis);
    
         },
         error: function() {
             errorMsg('Change Location', 'tdere has been an error, please alert us immediately');
             buttonLoading('reset', $tdis);
         }
    
     });

    return false;
});


            
});
</script>
@stop
