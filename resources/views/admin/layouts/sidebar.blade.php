<style>
    .sidebar-navs a, button {
    background: rgba(255,255,255,0.1);
    border: 1px solid #eaedf1 !important;
    color: #68798b !important;
    border-radius: 5px;
    padding: 0.8rem !important;
}
</style>

<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="side-header">
        <a class="header-brand1" href="index.html">
            <img src="https://www.tatagreenbattery.com/wp-content/themes/tata-green/images/logo.png" class="header-brand-img desktop-logo" alt="logo">
            <img src="https://www.tatagreenbattery.com/wp-content/themes/tata-green/images/logo.png" class="header-brand-img toggle-logo" alt="logo">
            <img src="https://www.tatagreenbattery.com/wp-content/themes/tata-green/images/logo.png" class="header-brand-img light-logo" alt="logo">
            <img src="https://www.tatagreenbattery.com/wp-content/themes/tata-green/images/logo.png" class="header-brand-img light-logo1" alt="logo">
        </a><!-- LOGO -->
        <a aria-label="Hide Sidebar" class="app-sidebar__toggle ml-auto" data-toggle="sidebar" href="#"></a><!-- sidebar-toggle-->
    </div>
    <div class="app-sidebar__user">
        <div class="dropdown user-pro-body text-center">
            <div class="user-pic">
                <img src="{{ asset(auth()->user()->profile_image) }}" alt="user-img" class="avatar-xl rounded-circle">
            </div>
            <div class="user-info">
                <h6 class=" mb-0 text-dark">{{auth()->user()->name}}</h6>
                <span class="text-muted app-sidebar__user-name text-sm">{{auth()->user()->getRoleNames()}}</span>
            </div>
        </div>
    </div>
    <div class="sidebar-navs">
        <ul class="nav  nav-pills-circle">
            <li class="nav-item" data-toggle="tooltip" data-placement="top" title="Settings">
                <a class="nav-link text-center m-2" href="{{ route('setting') }}">
                    <i class="fe fe-settings"></i>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="top" title="Chat">
                <a class="nav-link text-center m-2">
                    <i class="fe fe-mail"></i>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="top" title="Profile">
                <a class="nav-link text-center m-2"  href="{{ route('setting') }}">
                    <i class="fe fe-user"></i>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="top" title="Logout">
                <form action="{{route('logout')}}" method="post">
                    @csrf
                    <input type="hidden" name="status" value="1" />
                    <button type="submit" ><i class="fe fe-power"></i></button>
                </form>
            </li>
        </ul>
    </div>


    <ul class="side-menu">
        <li>
            <a class="side-menu__item" href="{{ route('dashboard') }}"><i class="side-menu__icon ti-home"></i><span class="side-menu__label">Dashboard</span></a>
        </li>

       
         @can('User Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Employees</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">
            @can('User List')
                <li>
                    <a href="{{ route('user-list') }}" class="slide-item">Employee List<a>
                </li>
            @endcan

            @can('User Create')
                <li>
                    <a href="{{ route('user-create') }}" class="slide-item">Create Employee<a>
                </li>
            @endcan
            </ul>
        </li>
        @endcan

        
        @can('User Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Roles</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">

            @can('User Master')
                <li>
                    <a href="{{ route('roles-list') }}" class="slide-item">Roles List<a>
                </li>
            @endcan

            @can('User Master')
                <li>
                    <a href="{{ route('roles-create') }}" class="slide-item">Roles Create<a>
                </li>
            @endcan
            </ul>
        </li>
        @endcan

        @can('Leads Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Leads</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">

            @can('Leads Master')
                <li>
                    <a href="{{ route('leads-list') }}" class="slide-item">Leads List<a>
                </li>
            @endcan
            
            @can('Leads Master')
                <li>
                    <a href="{{ route('open-list') }}" class="slide-item">Open Leads List<a>
                </li>
            @endcan
            
            @can('Leads Master')
                <li>
                    <a href="{{ route('closed-list') }}" class="slide-item">Closed Leads List<a>
                </li>
            @endcan
            
            @can('Leads Master')
                <li>
                    <a href="{{ route('cancelled-list') }}" class="slide-item">Cancelled Leads List<a>
                </li>
            @endcan
            
             @can('Leads Master')
                <li>
                    <a href="{{ route('re-scheduled-list') }}" class="slide-item">Re-scheduled Leads List<a>
                </li>
            @endcan
            
              @can('Leads Master')
                <li>
                    <a href="{{ route('first-Contact-list') }}" class="slide-item">1st Contact  List<a>
                </li>
            @endcan

            @if(auth()->user()->zonal_head=='Yes')
                @can('Leads Master')
                    <li>
                        <a href="#" class="slide-item">Leads List( Zonal )<a>
                    </li>
                @endcan
            @endif
         
            </ul>
        </li>
        @endcan


       {{-- @can('Leads Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Leads Status</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">

            @can('Leads List')
                <li>
                    <a href="{{ route('category-list') }}" class="slide-item">Status List<a>
                </li>
            @endcan

            @can('Leads Create')
                <li>
                    <a href="{{ route('category-create') }}" class="slide-item">Status Create<a>
                </li>
            @endcan
         
            </ul>
        </li>
        @endcan--}}
        
        @can('Escalation Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Escalation</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">

            @can('Escalation List')
                <li>
                    <a href="{{ route('escalation-list') }}" class="slide-item">Escalation List<a>
                </li>
            @endcan

            @can('Escalation Create')
                <li>
                    <a href="{{ route('escalation-create') }}" class="slide-item">Escalation Create<a>
                </li>
            @endcan
            </ul>
        </li>
        @endcan 

       {{-- @can('Vendor Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Vendor</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">

            @can('Vendor List')
                <li>
                    <a href="{{ route('vendor-list') }}" class="slide-item">Vendor List<a>
                </li>
            @endcan

            @can('Vendor Create')
                <li>
                    <a href="{{ route('vendor-create') }}" class="slide-item">Vendor Create<a>
                </li>
            @endcan
            </ul>
        </li>
        @endcan 

         @if(auth()->user()->hasRole('Employee') || auth()->user()->hasRole('Super Admin'))

    @can('PO Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">PO</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">
            @can('PO List')
                <li>
                    <a href="{{ route('po-list') }}" class="slide-item">PO List<a>
                </li>
            @endcan

            @can('PO Create')
                <li>
                    <a href="{{ route('po-create') }}" class="slide-item">Create PO<a>
                </li>
            @endcan

            @can('PO List')
            <li>
                <a href="{{ route('po-master') }}" class="slide-item">PO Master<a>
            </li>
            <li>
                <a href="{{ route('payment-master') }}" class="slide-item">Payment Master<a>
            </li>
            <li>
                <a href="{{ route('income-master') }}" class="slide-item">Income Master<a>
            </li>
            @endcan


            </ul>
        </li>
        @endcan
        @endif

        @can('Tax Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Tax</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">

            @can('Tax List')
                <li>
                    <a href="{{ route('tax-list') }}" class="slide-item">Tax List<a>
                </li>
            @endcan

            @can('Tax Create')
                <li>
                    <a href="{{ route('tax-create') }}" class="slide-item">Tax Create<a>
                </li>
            @endcan
            </ul>
        </li>
        @endcan

        @if(auth()->user()->hasRole('Employee'))
            @can('Amendment Master')
            <li class="slide">
                <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                    <span class="side-menu__label">PO Amendment</span>
                    <i class="angle fa fa-angle-right"></i>
                </a>
                <ul class="slide-menu">
                @can('Amendment List')
                    <li>
                        <a href="{{ route('amendment-list') }}" class="slide-item">PO Amendment List<a>
                    </li>
                @endcan

                @can('Amendment Create')
                    <li>
                        <a href="{{ route('amendment-create') }}" class="slide-item">Create PO Amendment<a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan

            @can('Vendor Code Master')
            <li class="slide">
                <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                    <span class="side-menu__label">Vendor</span>
                    <i class="angle fa fa-angle-right"></i>
                </a>
                <ul class="slide-menu">
                @can('Vendor Code List')
                    <li>
                        <a href="{{ route('vendor-code-list') }}" class="slide-item">Vendor List<a>
                    </li>
                @endcan

                @can('Vendor Code Create')
                    <li>
                        <a href="{{ route('vendor-code-create') }}" class="slide-item">Create Vendor<a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan

            @can('Service Master')
            <li class="slide">
                <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                    <span class="side-menu__label">Service</span>
                    <i class="angle fa fa-angle-right"></i>
                </a>
                <ul class="slide-menu">
                @can('Service List')
                    <li>
                        <a href="{{ route('service-list') }}" class="slide-item">Service List<a>
                    </li>
                @endcan

                @can('Service Create')
                    <li>
                        <a href="{{ route('service-create') }}" class="slide-item">Create Service<a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan
        @endif

        @can('Approval Master')
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon ti-user"></i>
                <span class="side-menu__label">Approval</span>
                <i class="angle fa fa-angle-right"></i>
            </a>
            <ul class="slide-menu">
            @can('Approval List')
                <li>
                    <a href="{{ route('approval-list') }}" class="slide-item">Approval List<a>
                </li>
            @endcan

            @can('Approval Create')
                <li>
                    <a href="{{ route('approval-create') }}" class="slide-item">Create Approval<a>
                </li>
            @endcan
            </ul>
        </li>
        @endcan --}}

    </ul>

</aside>
