@extends('admin/layouts/default')
@section('title')
<title>Roles | Fcs-Dashboard</title>
@stop
@section('inlinecss')

@stop
@section('content')
<div class="app-content">
    <div class="side-app">

        <!-- PAGE-HEADER -->
        @include('admin.layouts.pagehead')
        <!-- PAGE-HEADER END -->

        <!-- ROW-1 OPEN -->
        <div class="col-12">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Roles</h3>
                        <div class="ml-auto pageheader-btn">
								<a href="{{URL::to('roles/create')}}" class="btn btn-success btn-icon text-white mr-2">
									<span>
										<i class="fe fe-plus"></i>
									</span> Add Role
								</a>
								<a href="#" class="btn btn-danger btn-icon text-white">
									<span>
										<i class="fe fe-log-in"></i>
									</span> Export
								</a>
							</div>
                    </div>
                    <div class="card-body ">

                    <table class="table table-bordered data-table">
                      <thead>
                          <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Description</th>
                              <th>Created By</th>
                              <th>Created On</th>
                              <th width="100px">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $key=>$val)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>@if(!empty($val->name)){{$val->name}}@endif</td>
                            <td>@if(!empty($val->description)){{$val->description}}@endif</td>
                            <td>@if(!empty($val->created_at)){{$val->created_by}}@endif</td>
                            <td>@if(!empty($val->created_at)){{$val->created_at}}@endif</td>
                            <td  class="text-left">
                                <a href="{{route("roles-edit", $val->id)}}" class="edit btn btn-primary btn-sm">Edit</a>
                                <button type="button" data-url="{{route('roles-view', $val->id)}}" class="edit btn-sm btn btn-primary btn-sm viewDetail">View</button>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                  </table>

                    </div>
                </div>
            </div>
        </div>
        <!-- ROW-1 CLOSED -->
    </div>





</div>
@stop
@section('inlinejs')
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(function () {
           var table = $('.data-table').DataTable();
        });
    </script>
@stop
