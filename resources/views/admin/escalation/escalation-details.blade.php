@extends('admin/layouts/default')
@section('title')
<title>Leads Details </title>
@stop
@section('inlinecss')
<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/ui-lightness/jquery-ui.css" />
@stop
@section('breadcrum')
<h1 class="page-title">Leads Details </h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Leads </a></li>
    <li class="breadcrumb-item active" aria-current="page">Details</li>
</ol>
@stop
@section('content')
<style>
#ui-datepicker-div
{
z-index: 999999!important;
}
</style>
<div class="app-content">
    <div class="side-app">
        @include('admin.layouts.pagehead')
        @include('admin.alert')
        <br />
        <div class="card p-3 pt-3" style="overflow: scroll">
             <h4><b>Leads  Detail</b> </h4>

            <table class="table table-bordered data-table">
                <tbody>
                    <tr>
                       <th>User Name</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->name}}</b></td>
                     </tr>

                    <tr>
                       <th> E-mail</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->email}}</b></td>
                     </tr>
                     
                    <tr>
                       <th> Mobile No</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->mobile}}</b></td>
                     </tr>
                     
                  
                    <tr>
                       <th>States</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->state}}</b></td>
                     </tr>

                    <tr>
                       <th>City</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->city}}</b></td>
                     </tr>

                    <tr>
                       <th>Category</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->category}}</b></td>
                     </tr>

                    <tr>
                       <th>Company</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->company}}</b></td>
                    </tr>

                    <tr>
                       <th>Model</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->model}}</b></td>
                    </tr>

                    <tr>
                       <th>Battery Type</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->battery_type}}</b></td>
                    </tr>

                    <tr>
                       <th>Warranty</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->warranty}}</b></td>
                    </tr>
                    
                    <tr>
                       <th>MRP</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->mrp}}</b></td>
                    </tr>

                    <tr>
                       <th>Capacity</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->capacity}}</b></td>
                    </tr>

                    <tr>
                       <th>Group Company</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->group_company}}</b></td>
                    </tr>

                    <tr>
                       <th>How Do You Know</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->how_do_you_know}}</b></td>
                    </tr>
    
                    <tr>
                       <th>Lead From</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->Lead_from}}</b></td>
                    </tr>

                     <tr>
                       <th>Lead Status</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->lead_status}}</b></td>
                    </tr>
                    
                     <tr>
                       <th>Created At</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$leads->created_at}}</b></td>
                    </tr>
                    
                    <tr>
                       <th>Lead Status</th>
                       <td colspan="12">
                           <button type="button" class="btn btn-primary statusDetail btn-sm" data-toggle="modal" data-target="#statusDetail">Update Status</button>
                       </td>
                    </tr>
                    
                </tbody>
              </table>
        </div>
          <div class="col-12">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Status Update List</h3>
                    </div>
                    <div class="card-body ">

                    <table class="table table-bordered data-table">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Status</th>
                             
                              <th>Comment</th>
                              <th>Reminder Date </th>
                              <th>Created At</th>
                              <th>Updated At</th>
                              <!--<th width="100px">Action</th>-->
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($statusupdate as $key=>$val)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$val->status_cat_name}}</td>
                            <td>{{$val->comment}}</td>
                            <td>{{$val->reminder_date}}</td>
                            <td>{{$val->created_at}}</td>
                            <td>{{$val->updated_at}}</td>
                           
                        </tr>
                        @endforeach
                      </tbody>
                  </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="statusDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Status Update</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form  id="submitForm" action="{{route('status.update',$leads->id)}}">
			    @csrf
    			<div class="modal-body">
    			    <div class="form-group">
    			        <label>Status</label>
    			        <select class="form-control" name="status_cat_id" id="status_cat_id" onchange="getSubStatus()">
    			            <option value="">Select</option>
    			            @foreach($category as $key=>$val)
        			            <option value="{{$val->id}}" @if($val->title==$leads->lead_status) selected @endif data-status-name="{{$val->title}}">{{$val->title}}</option>
                            @endforeach
    			        </select>
    			    </div>

           <!--         <div class="form-group">-->
    			    <!--    <label>Sub Status</label>-->
    			    <!--    <select  class="form-control" name="status_subcat" id="status_subcat">-->
    			    <!--        <option value="">Select</option>-->
    			    <!--    </select>-->
    			    <!--</div>-->

                    <div class="form-group">
    			        <label>Comment</label>
                        <textarea name="comment" class="form-control" id="comment"></textarea>
    			    </div>

                    <div class="form-group d-none" id="reminder-div">
    			        <label>Reminder Date</label>
    			        <input type="text" class="form-control" name="reminder_date" id="reminder_date" />
    			    </div>
    			    
                </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
				 <button type="submit" id="submitButton2" class=" text-white btn btn-primary float-right"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Sending..." data-rest-text="Submit">Submit</button>
				
			</div>
    			<input type="hidden" name="status_cat_name" id="status_cat_name" />
			</form>
		</div>
	</div>
</div>

@stop
@section('inlinejs')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#reminder_date" ).datepicker({
    //comment the beforeShow handler if you want to see the ugly overlay
    beforeShow: function() {
        setTimeout(function(){
            $('.ui-datepicker').css('z-index', 99999999999999);
        }, 0);
    }
});
  } );
  </script>
  

<script>


$('#pdfprint').click(function () {
window.print()
});
    
    function getSubStatus(){
        
        var  status_cat_id = $("#status_cat_id option:selected").val();
        var status_cat_name =      $("#status_cat_id option:selected").data('status-name');
            $("#status_cat_name").val($("#status_cat_id option:selected").data('status-name'));
        
            
             if(status_cat_name=='Reshedued')
             {
                 $("#reminder-div").removeClass('d-none');
             }
             else
             {
                 $("#reminder-div").addClass('d-none');                 
             }
    //     $.ajax({
    //      url: '{{route('get.substatus')}}',
    //      type: "GET",
    //      data: {status_cat_id : status_cat_id},
    //      success: function(data) 
    //      {
    //           $("#status_subcat").empty();
    //             $("#status_subcat").append(`<option value="">Select</option>`);
    //         if(data.datas.length>0)
    //         {
               
    //             for(var i=0;i<data.datas.length;i++)
    //             {
    //                 $("#status_subcat").append(`<option value="${data.datas[i].title}">${data.datas[i].title}</option>`);
    //             }
    //         }
    
    //      }
    
    //  });
    }

     $(function () {

$('#submitForm').submit(function(){
     var $this = $('#submitButton');
     buttonLoading('loading', $this);
     $('.is-invalid').removeClass('is-invalid state-invalid');
     $('.invalid-feedback').remove();
     $.ajax({
         url: $('#submitForm').attr('action'),
         type: "POST",
         processData: false,  // Important!
         contentType: false,
         cache: false,
         data: new FormData($('#submitForm')[0]),
         success: function(data) {
             if(data.status){
                 var btn = '<a href="#" class="btn btn-info btn-sm">GoTo List</a>';
                 successMsg('Status Update', data.msg, btn);
                 $('#submitForm')[0].reset();
    
             }else{
    
                 $.each(data.errors, function(fieldName, field){
                     $.each(field, function(index, msg){
                         $('#'+fieldName).addClass('is-invalid state-invalid');
                        errorDiv = $('#'+fieldName).parent('div');
                        errorDiv.append('<div class="invalid-feedback">'+msg+'</div>');
                     });
                 });
                 errorMsg('Status Update', 'Input Error');
             }
             buttonLoading('reset', $this);
    
         },
         error: function() {
             errorMsg('Status Update', 'There has been an error, please alert us immediately');
             buttonLoading('reset', $this);
         }
    
     });

    return false;
});


            
});
</script>
@stop
