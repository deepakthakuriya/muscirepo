@extends('admin/layouts/default')
@section('title')
<title>Escalation List</title>
@stop
@section('inlinecss')

@stop
@section('content')
<div class="app-content">
    <div class="side-app">

        <!-- PAGE-HEADER -->
        @include('admin.layouts.pagehead')
        <!-- PAGE-HEADER END -->

        <!-- ROW-1 OPEN -->
        <div class="col-12">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Escalation List</h3>
                        <div class="ml-auto pageheader-btn">
                           
							</div>
                    </div>
                    <div class="card-body ">

                    <table class="table table-bordered data-table">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>State</th>
                              <th>Name 1</th>
                              <th>E-mail 1</th>
                              <th>Name 2</th>
                              <th>E-mail 2</th>
                              <th>Created At</th>
                              <th width="100px">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($escalation as $key=>$val)
                        <tr>
                            <td>{{$val->id}}</td>
                            <td>{{$val->state}}</td>
                            <td>{{$val->name_1}}</td>
                            <td>{{$val->email_1}}</td>
                            <td>{{$val->name_2}}</td>
                            <td>{{$val->email_2}}</td>
                            <td>{{$val->created_at}}</td>
                            <td>
                                <a href="{{route('escalation-edit',$val->id)}}" class="btn btn-sm btn-primary">Edit</a>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                  </table>

                    </div>
                </div>
            </div>
        </div>
        <!-- ROW-1 CLOSED -->
    </div>

    <!-- View MODAL -->
<div class="modal fade" id="viewDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

			</div>
		</div>
	</div>
</div>
<!-- View CLOSED -->





</div>
@stop
@section('inlinejs')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $.fn.dataTable.ext.errMode = 'none';
            var table = $('.data-table').DataTable();



        });
    </script>
@stop
