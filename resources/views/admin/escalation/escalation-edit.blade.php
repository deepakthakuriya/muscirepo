@extends('admin/layouts/default')
@section('title')
<title>Edit Category</title>
@stop

@section('inlinecss')
<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/ui-lightness/jquery-ui.css" />
<link href="{{ asset('admin/assets/multiselectbox/css/ui.multiselect.css') }}" rel="stylesheet">
@stop

@section('breadcrum')
<h1 class="page-title">Edit Category</h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Category</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit</li>
</ol>
@stop

@section('content')
<div class="app-content">
    <div class="side-app">

        <!-- PAGE-HEADER -->
        @include('admin.layouts.pagehead')

    <form id="submitForm" class="row"  method="post" action="{{route('escalation-update', $loan->id)}}">
        {{csrf_field()}}
    	<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="form-group">
						
                        <div class="form-group">
								<label class="form-label">States *</label>
								<select name="state" id="state"   class="form-control">
									<option value="">Select</option>
                                    @foreach($states as $key=>$val)
										<option @if($val->name==$loan->state) selected @endif value="{{$val->name}}">{{$val->name}}</option>
                                    @endforeach
								</select>
							</div>
					</div>
				</div>
			</div>
        </div>
        
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Escalation 1</h3>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label class="form-label">Name *</label>
						<input type="text" class="form-control" name="name_1" id="name_1" placeholder="Name.." value="{{$loan->name_1}}">
					</div>

                	<div class="form-group">
						<label class="form-label">E-mail *</label>
						<input type="text" class="form-control" name="email_1" id="email_1" placeholder="Name.." value="{{$loan->email_1}}">
					</div>
					
					</div>
					
					 

				</div>
				
				
		</div>
    	<div class="col-lg-12">
    			<div class="card">
    				<div class="card-header">
    					<h3 class="card-title">Escalation 2</h3>
    				</div>
    				<div class="card-body">
    					<div class="form-group">
    						<label class="form-label">Name *</label>
    						<input type="text" class="form-control" name="name_2" id="name_2" placeholder="Name.." value="{{$loan->name_2}}">
    					</div>
    
                    	<div class="form-group">
    						<label class="form-label">E-mail *</label>
    						<input type="text" class="form-control" name="email_2" id="email_2" placeholder="Name.." value="{{$loan->email_2}}">
    					</div>
    					
    				</div>
    				
    				<div class="card-footer">
                        <button type="submit" id="submitButton" class="btn btn-primary float-right"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Sending..." data-rest-text="Update">Update</button>                                            
                    </div>
        
    			</div>
    				
    		</div>
    		
		

	</form>
        </div>
    </div>
</div>

@stop
@section('inlinejs')
    <script type="text/javascript">
 $('.Description').summernote({ height:250 });
        $(function () {
           $('#submitForm').submit(function(){
            var $this = $('#submitButton');
            buttonLoading('loading', $this);
            $('.is-invalid').removeClass('is-invalid state-invalid');
            $('.invalid-feedback').remove();
            $.ajax({
                url: $('#submitForm').attr('action'),
                type: "POST",
                processData: false,  // Important!
                contentType: false,
                cache: false,
                data: new FormData($('#submitForm')[0]),
                success: function(data) {
                    if(data.status){
						var btn = '<a href="{{route('escalation-list')}}" class="btn btn-info btn-sm">GoTo List</a>';
                        successMsg('Edit Category', data.msg, btn);


                    }else{
                        $.each(data.errors, function(fieldName, field){
                            $.each(field, function(index, msg){
                                $('#'+fieldName).addClass('is-invalid state-invalid');
                               errorDiv = $('#'+fieldName).parent('div');
                               errorDiv.append('<div class="invalid-feedback">'+msg+'</div>');
                            });
                        });
                        errorMsg('Edit Category','Input error');
                    }
                    buttonLoading('reset', $this);

                },
                error: function() {
                    errorMsg('Edit Category', 'There has been an error, please alert us immediately');
                    buttonLoading('reset', $this);
                }

            });

            return false;
           });

           });
		   $("#icon").change(function(){
                readURL(this);
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#icon_image_select').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }


    </script>
@stop
@section('bottomjs')

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script>
<script src="{{ asset('admin/assets/multiselectbox/js/ui.multiselect.js') }}"></script>
<script>

$(function () {
  $('#loan_fields').multiselect();
  $("ul.selected li").each(function(){
		var selected_value = $(this).attr('data-selected-value');
		//alert(selected_value);
	});
});
  </script>
@stop
