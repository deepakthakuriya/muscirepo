@extends('admin/layouts/default')
@section('title')
<title>Create Category</title>
@stop

@section('inlinecss')
<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/ui-lightness/jquery-ui.css" />
<link href="{{ asset('admin/assets/multiselectbox/css/ui.multiselect.css') }}" rel="stylesheet">
@stop

@section('breadcrum')
<h1 class="page-title">Create Category</h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Category</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
</ol>
@stop

@section('content')
<div class="app-content">
    <div class="side-app">
        @include('admin.layouts.pagehead')
            <form id="submitForm" class="row"  method="post" action="{{route('escalation-save')}}">
                {{csrf_field()}}
                
            	<div class="col-lg-12">
        			<div class="card">
        				<!--<div class="card-header">-->
        				<!--	<h3 class="card-title">State</h3>-->
        				<!--</div>-->
        				<div class="card-body">
        					<div class="form-group">
        						
                                <div class="form-group">
										<label class="form-label">States *</label>
										<select name="state" id="state"   class="form-control">
											<option value="">Select</option>
                                            @foreach($states as $key=>$val)
												<option value="{{$val->name}}">{{$val->name}}</option>
                                            @endforeach
										</select>
									</div>
        					</div>
        				</div>
        			</div>
                </div>
                
        		<div class="col-lg-12">
        			<div class="card">
        				<div class="card-header">
        					<h3 class="card-title">Escalation 1</h3>
        				</div>
        				<div class="card-body">
        					<div class="form-group">
        						<label class="form-label">Name *</label>
        						<input type="text" class="form-control" name="name_1" id="name_1" placeholder="Name.." >
        					</div>
        
                        	<div class="form-group">
        						<label class="form-label">E-mail *</label>
        						<input type="text" class="form-control" name="email_1" id="email_1" placeholder="Name.." >
        					</div>
        					
        					</div>
        				</div>
                </div>
                
            	<div class="col-lg-12">
            			<div class="card">
            				<div class="card-header">
            					<h3 class="card-title">Escalation 2</h3>
            				</div>
            				<div class="card-body">
            					<div class="form-group">
            						<label class="form-label">Name *</label>
            						<input type="text" class="form-control" name="name_2" id="name_2" placeholder="Name..">
            					</div>
            
                            	<div class="form-group">
            						<label class="form-label">E-mail *</label>
            						<input type="text" class="form-control" name="email_2" id="email_2" placeholder="Name..">
            					</div>
            					
            				</div>
            				
            				<div class="card-footer">
                                <button type="submit" id="submitButton" class="btn btn-primary float-right"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Sending..." data-rest-text="Create">Create</button>                                            
                            </div>
                
            			</div>
            				
                </div>
        	</form>
        </div>
    </div>
</div>

@stop
@section('inlinejs')
<script type="text/javascript">
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#icon_image_select').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
   $('.Description').summernote({ height:250 });
        $(function () {
           $('#submitForm').submit(function(){
            var $this = $('#submitButton');
            buttonLoading('loading', $this);
            $('.is-invalid').removeClass('is-invalid state-invalid');
            $('.invalid-feedback').remove();
            $.ajax({
                url: $('#submitForm').attr('action'),
                type: "POST",
                processData: false,  // Important!
                contentType: false,
                cache: false,
                data: new FormData($('#submitForm')[0]),
                success: function(data) {
                    if(data.status){
						var btn = '<a href="{{route('category-list')}}" class="btn btn-info btn-sm">GoTo List</a>';
                        successMsg('Create Category', data.msg, btn);
                        $('#submitForm')[0].reset();

                    }else{
                        $.each(data.errors, function(fieldName, field){
                            $.each(field, function(index, msg){
                                $('#'+fieldName).addClass('is-invalid state-invalid');
                               errorDiv = $('#'+fieldName).parent('div');
                               errorDiv.append('<div class="invalid-feedback">'+msg+'</div>');
                            });
                        });
                        errorMsg('Create Category','Input error');
                    }
                    buttonLoading('reset', $this);

                },
                error: function() {
                    errorMsg('Create Category', 'There has been an error, please alert us immediately');
                    buttonLoading('reset', $this);
                }

            });

            return false;
           });

           });


    </script>
@stop
