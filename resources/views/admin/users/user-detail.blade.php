@extends('admin/layouts/default')
@section('title')
<title>View User Details </title>
@stop
@section('inlinecss')
<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/ui-lightness/jquery-ui.css" />
<link href="{{ asset('admin/assets/multiselectbox/css/ui.multiselect.css') }}" rel="stylesheet">
@stop
@section('breadcrum')
<h1 class="page-title">View User Details </h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Orders </a></li>
    <li class="breadcrumb-item active" aria-current="page">View</li>
</ol>
@stop
@section('content')
<div class="app-content">
    <div class="side-app">
        <!-- PAGE-HEADER -->
        @include('admin.layouts.pagehead')
        <!-- PAGE-HEADER END -->

            @include('admin.alert')
<br />
        <div class="card p-3 pt-3" style="overflow: scroll">
            {{--<span class="float-right"><button class="btn btn-info" id="pdfprint">Pdf Print </button></span>--}}
             <h4><b>View  Detail</b> </h4>
             <!--{{--@php-->
             <!--echo "<pre>";print_r($user);-->
             <!--@endphp--}}-->
            <table class="table table-bordered data-table">
                <tbody>
                    <tr>
                       <th>User Name</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$user->name}}</b></td>
                     </tr>

                    <tr>
                       <th> E-mail</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$user->email}}</b></td>
                     </tr>
                     
                    <tr>
                       <th> Mobile No</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$user->mobile_no}}</b></td>
                     </tr>
                     
                    <tr>
                       <th> Profile Image</th>
                       <td colspan="12"> <b style="font-weight: 1000;"><img src="{{url($user->profile_image)}}" /></b></td>
                     </tr>

                    <tr>
                       <th>Business Name</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$user->business_name}}</b></td>
                     </tr>

                    <tr>
                       <th>Store Name</th>
                       <td colspan="12"> <b style="font-weight: 1000;">{{$user->store_name}}</b></td>
                     </tr>


                </tbody>
              </table>
        </div>
    </div>
            </div>

@stop
@section('inlinejs')
<script>
$('#pdfprint').click(function () {
window.print()
});


     $(function () {

$('#submitForm').submit(function(){
 var $this = $('#submitButton');
 buttonLoading('loading', $this);
 $('.is-invalid').removeClass('is-invalid state-invalid');
 $('.invalid-feedback').remove();
 $.ajax({
     url: $('#submitForm').attr('action'),
     type: "POST",
     processData: false,  // Important!
     contentType: false,
     cache: false,
     data: new FormData($('#submitForm')[0]),
     success: function(data) {
         if(data.status){
             var btn = '<a href="#" class="btn btn-info btn-sm">GoTo List</a>';
             successMsg('Edit Order', data.msg, btn);
             $('#submitForm')[0].reset();

         }else{

             $.each(data.errors, function(fieldName, field){
                 $.each(field, function(index, msg){
                     $('#'+fieldName).addClass('is-invalid state-invalid');
                    errorDiv = $('#'+fieldName).parent('div');
                    errorDiv.append('<div class="invalid-feedback">'+msg+'</div>');
                 });
             });
             errorMsg('Edit Order', 'Input Error');
         }
         buttonLoading('reset', $this);

     },
     error: function() {
         errorMsg('Edit Story', 'There has been an error, please alert us immediately');
         buttonLoading('reset', $this);
     }

 });

 return false;
});

});
</script>
@stop
