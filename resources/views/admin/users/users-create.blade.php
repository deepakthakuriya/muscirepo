@extends('admin/layouts/default')
@section('title')
<title>Create User</title>
@stop

@section('inlinecss')
<link href="{{ asset('admin/assets/multiselectbox/css/multi-select.css') }}" rel="stylesheet">
@stop

@section('breadcrum')
<h1 class="page-title">Create Users</h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
</ol>
@stop

@section('content')
<div class="app-content">
    <div class="side-app">
        @include('admin.layouts.pagehead')
							<div class="col-lg-8">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">User Forms</h3>
									</div>
									<div class="card-body">
                                    <form id="submitForm"  method="post" action="{{route('user-save')}}">
                                    {{csrf_field()}}

                                        <div class="form-group">
											<label class="form-label">Employee ID *</label>
											<input type="text" class="form-control" name="emp_id" id="emp_id" placeholder="Employee ID..">
										</div>


										<div class="form-group">
											<label class="form-label">Name *</label>
											<input type="text" class="form-control" name="name" id="name" placeholder="Name..">
										</div>

                                        <div class="form-group">
											<label class="form-label">Email *</label>
											<input type="email" class="form-control" name="email" id="email" placeholder="Email..">
										</div>

                                        <div class="form-group">
											<label class="form-label">Mobile No *</label>
											<input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile..">
                                        </div>


                                        <div class="form-group">
											<label class="form-label">States *</label>
											<select name="states_id[]" id="states_id"   class="form-control">
												<option value="">Select</option>
                                                @foreach($states as $key=>$val)
    												<option value="{{$val->name}}">{{$val->name}}</option>
                                                @endforeach
											</select>
										</div>

                                        
										

										<div class="form-group">
											<label class="form-label">Role *</label>
											<select name="roles"   class="form-control">
												<option value="">Select Role</option>
                                                @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
											</select>
										</div>


                                        <div class="form-group ">
                                            <div class="row">
                                            <div class="col-10">
                                                <label class="form-label">Profile Image</label>
                                                <input type="file" onchange="readURL(this,'file')" class="form-control" name="profile_image" id="profile_image" placeholder="Mobile..">
                                            </div>

                                            <div class="col-2">
                                                <img src="https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png" id="file" style="height: 73px;width: 73px;"/>
                                            </div>
                                        </div>
                                        </div>


                                        <div class="form-group">
											<label class="form-label">Password *</label>
											<div class="input-group">
												<input type="text" name="password" id="password" class="form-control" placeholder="">
												<span class="input-group-append">
													<button class="btn btn-sm btn-primary" type="button" onclick="getPassword()">Generate!</button>
												</span>
											</div>


                                        </div>
                                        
                                       
										
                                        
                                        
                                        <div class="form-group col-12">
											<label class="form-label">Status</label>
											<select name="status" id="status" class="form-control ">
												<option value="1">Active</option>
												<option value="0">InActive</option>
											</select>
                                        </div>
                                        
                                        <div class="card-footer"></div>
                                            <button type="submit" id="submitButton" class="btn btn-primary btn-sm float-right"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Sending..." data-rest-text="Create">Create</button>
                                        </div>
                                        
                                        <input type="hidden" name="states_name" id="states_name" />
                                     </form>
									</div>

								</div>
							</div><!-- COL END -->

        <!--  End Content -->

    </div>
</div>

@stop
@section('inlinejs')
<script src="{{ asset('admin/assets/multiselectbox/js/jquery.multi-select.js') }}"></script>
    <script type="text/javascript">
    
   
function readURL(input, imgId) {
   if (input.files && input.files[0]) {
       var reader = new FileReader();
       reader.onload = function (e) {
           $('#' + imgId).attr('src', e.target.result);
       };
       reader.readAsDataURL(input.files[0]);
   }
}

  


$('#states_id').multiSelect();
$('#zonal_states_id').multiSelect();

$('#roles').multiSelect({
  afterSelect: function(values){
    if(values==4){
        $("#store_div").removeClass('d-none');
        $("#business_div").removeClass('d-none');
    }
    else if(values==2){
        $("#business_div").removeClass('d-none');
    }
    $("#usertype").val($('#roles option:selected').text());
  },
  afterDeselect: function(values){
    $("#usertype").val('');
    if(values==4){
        $("#store_div").addClass('d-none');
        $("#business_div").addClass('d-none');
    }
    else if(values==2){
        $("#business_div").addClass('d-none');
    }

  }
});



           $('#submitForm').submit(function(){
            var $this = $('#submitButton');
            buttonLoading('loading', $this);
            $('.is-invalid').removeClass('is-invalid state-invalid');
            $('.invalid-feedback').remove();
            $.ajax({
                url: $('#submitForm').attr('action'),
                type: "POST",
                processData: false,  // Important!
                contentType: false,
                cache: false,
                data: new FormData($('#submitForm')[0]),
                success: function(data) {
                    if(data.status){

                        successMsg('Create User', 'User Created...');
                        $('#submitForm')[0].reset();

                    }else{
                        $.each(data.errors, function(fieldName, field){
                            $.each(field, function(index, msg){
                                $('#'+fieldName).addClass('is-invalid state-invalid');
                               errorDiv = $('#'+fieldName).parent('div');
                               errorDiv.append('<div class="invalid-feedback">'+msg+'</div>');
                            });
                        });
                        errorMsg('Create User','Input error');
                    }
                    buttonLoading('reset', $this);

                },
                error: function() {
                    errorMsg('Create User', 'There has been an error, please alert us immediately');
                    buttonLoading('reset', $this);
                }

            });

            return false;
           });



       function getPassword()
       {
           pass=  Math.random().toString(36).slice(-8);
           $('#password').val(pass);
       }
       
       function getcities()
       {
           $("#states_name").val($("#states_id option:selected").text());
           
           $.ajax({
                url: '{{route('cities-list')}}',
                type: "GET",
                data: {state_id:$("#states_id").val()},
                success: function(data) 
                {
                    $("#city").empty();

                    $("#city").append(`<option value=''>Select</option>`);

                    for(var i=0;i<data.length;i++)
                    {
                        $("#city").append(`<option value="${data[i].city}">${data[i].city}</option>`);
                    }
                    
                }

            });
       }
       
    </script>
@stop
