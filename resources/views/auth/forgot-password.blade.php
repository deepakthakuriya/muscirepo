<!doctype html>
<html lang="en" dir="ltr">

<head>

    <!-- META DATA -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('admin/assets/images/brand/favicon.ico') }}" />

    <!-- TITLE -->
    <title>Aamod-Admin</title>

    <!-- BOOTSTRAP CSS -->
    <link href="{{ asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- STYLE CSS -->
    <link href="{{ asset('admin/assets/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/css/skin-modes.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/css/dark-style.css') }}" rel="stylesheet" />

    <!-- SINGLE-PAGE CSS -->
    <link href="{{ asset('admin/assets/plugins/single-page/css/main.css') }}" rel="stylesheet" type="text/css">
    <!--- FONT-ICONS CSS -->
    <link href="{{ asset('admin/assets/css/icons.css') }}" rel="stylesheet" />
    <!-- COLOR SKIN CSS -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ asset('admin/assets/colors/color1.css') }}" />

</head>

<body>

    <!-- BACKGROUND-IMAGE -->
    <div class="login-img">
        {{-- <div id="global-loader">
            <img src="{{ asset('admin/assets/images/loader.svg')}}" class="loader-img" alt="Loader">
        </div> --}}
        <!-- PAGE -->
        <div class="page">
            <div class="">
                <!-- CONTAINER OPEN -->
                <div class="col col-login mx-auto">
                    <div class="text-center">
                        <img src="{{ asset('admin/assets/images/brand/logo.png')}}" class="header-brand-img" alt="">
                    </div>
                </div>
                <div class="container-login100">
                    <div class="wrap-login100 p-6">
					
                        <form class="login100-form validate-form" method="POST" action="{{ route('send-email') }}">
                            @csrf
                           @if (\Session::has('message'))
                            <div class="alert alert-success">
                            <ul>
                            <li>{!! \Session::get('message') !!}</li>
                            </ul>
                            </div>
                            @endif
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" class="form-control" name="email" id="email"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- CONTAINER CLOSED -->
            </div>
        </div>
        <!-- End PAGE -->

    </div>
    <!-- BACKGROUND-IMAGE CLOSED -->

    <!-- JQUERY JS -->
    <script src="{{ asset('admin/assets/js/jquery-3.4.1.min.js') }}"></script>
    <input type="hidden" name="status" value="@if(isset($status)){{$status}}@endif" />
    <!-- BOOTSTRAP JS -->
    <script src="{{ asset('admin/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>