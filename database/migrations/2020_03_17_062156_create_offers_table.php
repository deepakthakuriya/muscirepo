<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('business_id');
            $table->string('business_name',50);
            $table->string('offer_name',50);
            $table->text('offer_image');
            $table->string('offer_desc');
            $table->string('offer_type');
            $table->string('offer_type_amt_code');
            $table->string('offer_discount_type');
            $table->string('discount_amountoff');
            $table->string('discount_percentage')->nullable();
            $table->string('receipt_amt');
            $table->string('receipt_day');
            $table->string('receipt_month');
            $table->string('stores');
            $table->string('offer_start_date');
            $table->string('offer_end_date')->nullable();
            $table->string('never_expire');
            $table->timestamps();
            //until sell how much you sell
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
